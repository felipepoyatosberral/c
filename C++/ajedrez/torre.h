
#ifndef TORRE_H
#define TORRE_H


/**
  * class Torre
  * 
  */

class Torre
{
public:
    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Torre();

    /**
     * Empty Destructor
     */
    virtual ~Torre();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //

protected:
    // Static Protected attributes
    //  

    // Protected attributes
    //  


    // Protected attribute accessor methods
    //  


    // Protected attribute accessor methods
    //

private:
    // Static Private attributes
    //  

    // Private attributes
    //  


    // Private attribute accessor methods
    //  


    // Private attribute accessor methods
    //

};

#endif // TORRE_H
