#include <iostream>
#include <string>

#include "pieza.h"
#include "peon.h"
#include "tablero.h"

using namespace std;

int main () {

	peon* peon1 = new peon("rojo", "peon1");
	peon* peon2 = new peon("verde", "peon2");

	peon1->nombre();
	peon2->nombre();	
}
