#ifndef PIEZA_H
#define PIEZA_H

#include <string>
#include "tablero.h"

using namespace std;

class Pieza
{

protected:

	string color;

public:
     
    Pieza(string color);    
    void setColor(string value);
    string getColor();

    virtual void mover(Tablero _tablero) = 0;

};

#endif // PIEZA_H
