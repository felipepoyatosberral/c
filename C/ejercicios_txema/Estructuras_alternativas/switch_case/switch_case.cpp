
#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>

#define MAX 20

int preguntar() {
	int opcion; //Variable donde guardamos la opcion que elije el usuario

	//Imprimimos el menu para que le usuario elija una opcion
	printf("1. Listar un directorio de su elección \n");
	printf("2. Ver los que ocupa un directorio y un subdirectorio \n");
	printf("3. Consultar la cantidad de espacio libre que tiene cada disco \n");
	printf("4. Mostrar la ocupación en memoria \n \n");
	
	//Le decimos al usuario que elija una opcion y la guardamos en la direccion de memoria de opcion
	printf("Elija una de estas 4 opciones (1-4): ");
	scanf(" %i", &opcion);

	return opcion; //Retornamos el contenido de opcion
}

void switch_case() {
	int opcion_elegida = preguntar(); //Guardamos lo que contiene opcion en opcion_elegida
	char comando[MAX] = "ls -lh"; //Guardamos el comando ls -lh en un array llamado comando
	char directorio_usuario[MAX]; //Guardamos el directorio que elije el usuario en un array llamado directorio_usuario

	switch(opcion_elegida){ //Hacemos un switch que depende de opcion_elegida
	
		case 1: //Primera opcion
			
			__fpurge(stdin); //Purgamos el stdin

			//Pedimos al usuario un directorio y lo guardamos en el array directorio_usuario
			printf("Introduce un directorio ");
			fgets (directorio_usuario, MAX, stdin);

			
			strcat (comando, " "); //Tenemos que poner el comando y un espacio para que no se ponga el comando y seguido el directorio, si no que haya un espacion entre ellos.
			strcat (comando, directorio_usuario); //Juntamos el comando con el espacio y el directorio que elija el usuario.
			
			system (comando); //Ejecutamos el comando con el directorio.
		break;

		case 2: //Segundo caso
			system("du -sh"); //Ejecutamos el comando du -sh
		break;

		case 3: //Tercer caso
			system("df -h"); //Ejecutamos el comando df -h
		break;

		case 4: //Cuarto caso
			system("free"); //Ejecutamos el comando free
		break;
	}
}

int main(){
	
	system("clear");

	switch_case();

	return EXIT_SUCCESS;
}

