
#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <cstring>

#define MAX 40

void array_while() {
	char frase[MAX]; //Creamos un array para guardar la frase que le pedimos al usuario
	int i=0; //Variable para la duracion del while

	__fpurge(stdin); //Purgamos el stdin

	//Le pedimos al usuario una frase y la introducimos en el array
	printf("Introduce una frase: ");
	fgets(frase, MAX, stdin);
	
	//Creamos una variable y le decimos que vale la longitud de la frase que hay en el array
	int espacio_array = strlen(frase);

	//Hacemos un while que dure hasta que i sea menor que la longitud de la frase
	while (i < espacio_array) {
	printf("%c ", frase[i]); //Printamos el caracter que se encuentra en la posicion i, i valdra el número de veces que se ejecute el while
	i++; //Incrementamos i cada vez que se ejecute el while
	}
}

void frase_alreves() {
	char frase[MAX]; //Creamos un array para guardar la frase que le pedimos al usuario
	int i=0; //Variable para la durecion del while

	__fpurge(stdin); //Purgamos el stdin

	//Pedimos una frase al usuario y la guardamos en el array
	printf("\nIntroduce una frase: ");
	fgets(frase, MAX, stdin);

	//Creamos una variable y le decimos que vale la longitud de la frase que hay en el array
	int espacio_array = strlen(frase);
	//Creamos una variable y le decimos que vale la longitud de la frase que hay en el array, le ponemos el -1 para que imprima todas las letras de la frase
	int alreves = strlen(frase) - 1;
	
	//Hacemos un while que dure hasta que i sea menos que la longitud de la frase
	while (i < espacio_array) {
        printf("%c ", frase[alreves]); //Printamos la frase pero la posicion del array la printamos desde el final hasta el principio
        i++; //Aumentamos i en uno
	alreves--; //Disminuimos alreves cada vez que se ejecute el while, esto lo hacemos para que la frase se vaya imprimiendo desde la ultima letra hasta la primera
        }
	printf("\n");
}

int main(){
	
	system("clear");

	array_while();
	frase_alreves();

	return EXIT_SUCCESS;

}

