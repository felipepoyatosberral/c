
#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

void title() {
	system("clear"); //Limpiamos la consola
	//Imprimimos el título del programa
	printf("**********************************\n");
	printf("**	   Funcion title        **\n");
	printf("**********************************\n");
}

int suma(int op1, int op2, int resultado) {
	//Le pedimos al usuario que introduzca un operando y lo guardamos en la direccion de memoria de op1
	printf("Introduce un operando: ");
	scanf(" %i", &op1);
	
	//Le pedimos al usuario que introduzca otro operando y lo guardamos en la direccion de memoria de op2
	printf("Introduce otro operando: ");
	scanf(" %i", &op2);

	//Igualamos la variable resultado a la suma de los dos operandos
	resultado = op1 + op2;
	
	return resultado; //Hacemos que la funcion retorne lo que vale la variable resultado
}

void pedir_entero(int *variable) {
	//Le pedimos al usuario un número entero y lo guardamos en variable
	printf("Introduce un número entero: ");
	scanf(" %i", variable);
}

int main(){
	int op1, op2, resultado;
	int variable;

	title();
	
	resultado = suma(op1, op2, resultado);
	printf("El resultado de la suma de los dos operandos es: %i \n \n", resultado);

	pedir_entero(&variable);
	return EXIT_SUCCESS;

}

