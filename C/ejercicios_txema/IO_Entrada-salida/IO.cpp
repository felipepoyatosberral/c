#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <stdio_ext.h>

#define MAX 30

void num_entero() {
	int num_entero; //Variable para el número entero que introduce el usuario

	//Pregunta el número al usuario y lo guarda en la dirección donde está num_entero
        printf("Introduce un número entero: ");
        scanf("%i", &num_entero);
	
	//Imprime el número que ha introducido el usuario
	printf("El número entero que has introducido es: %i \n \n", num_entero);
}

void num_decimal() {
	float num_decimal; //Variable para el número decimal que introduce el usuario

	//Pregutna el número al usuario y lo guarda en la dirección donde está num_decimal 
	printf("Introduce un número decimal: ");
	scanf("%f", &num_decimal);

	//Imprime el número que ha introducido el usuario
	printf("El número decimal que has introducido es: %.2f \n \n", num_decimal);

}

void num_int_char() {
	int num_entero; //Variable para el número entero que introduce el usuario
	char num_char; //Varible para imprimir el número como caracter
	
	//Preguntar al usuario por un numero entre 32 y 128 y guardarlo en la dirección donde esta num_entero
	printf("Introduce un número entre el 32 y el 128: ");
	scanf("%i", &num_entero);

	num_char == num_entero; //Pasar el número a caracter

	//Imprimir el número entero y el caracter
	printf("El número que has introducido es: %i \n", num_entero);
	printf("El número que has introducido es el caracter: %c \n \n", num_entero);

}

void array_hexa() {
	char array_hex[MAX]; //Array tipo caracter
	
	//Pregunta al usuario un número en hexadecimal y lo guarda en el array
	printf("Introduce un número en hexadecimal: ");
	scanf(" %[0-9a-f-A-F]", array_hex);
	
	//Imprime el número del usuario en el array
	printf("El numero que has introducido es: %s \n \n", array_hex);
}

void clean_ANSI() {
	printf("\033[2J");
}

int preguntar_op() {
	int num; //Variable para guardar el número
	static int var=0; //Variable que incrementa más adelante, la ponemos en statica ya que la segunda vez que se ejecute esta función no queremos que la variable empiece en 0.
	
	__fpurge(stdin);
	
	//Pregunta al usuario un operando, incrementa en 1 var y guarda el resultado en la dirección de memoria de num 
	printf("Introduce el %iº operando: ", ++var);
	scanf("%i", &num);
	
	return num;
}

void mostrar_suma(int op1, int op2, int resultado) {
	resultado = op1 + op2; //Realizar la suma de dos operandos

	printf("\x1b[96m%i + \x1b[96m%i = \x1b[32m%i \n", op1, op2, resultado); //Imprimir los operandos con el resultado en colores
}

void posicion_cursor() {
	
}

void leer_linea() {
	char array_linea[MAX]; //Creamos una variable de tipo carácter con un array de maximo 30 bytes
	
	__fpurge(stdin); //Purgamos en stdin para limpiar el tubo de entrada.
	
	printf("Introduce una frase: "); //Pedimos al usuario que introduzca una frase.
	fgets(array_linea, MAX, stdin); //Recogemos la frase que ha introducido el usuario con fgets y lo metemos en el array.

	char guardar_linea = strlen (array_linea); //Creamos una variable de tipo caracter que gracias al strlen equivale a los bytes que contiene en ese momento el array.
	array_linea[guardar_linea] = '\0';

	printf("La frase que has introducido es: "); //Imprimimos la frase que ha introducido el usuario con un printf y un puts con el array.
	puts(array_linea);
}

void hex_mayus() {
	int num_hex; //Creamos una variable de tipo caracter para el número hexadecimal

	printf("Introduce un número: "); //Le pedimos al usuario un número en hexadecimal.
	scanf(" %i", &num_hex); //Cogemos el numero que ha introducido el usuario y lo metemos en la dirección de memoria donde se encuentra la variable num_hex

	printf("El número que has introducido es: %X \n \n", num_hex); //Para imprimir un valor en hexadecimal utilizamos %x o %X, el primero convierte todas las letras en minúsculas y el segundo en mayúsculas.	
}

void direcc_mem(){
	int direcc;
	
	printf("La dirección de memoria de la variable es: %p \n \n", &direcc); //Imprimimos la dirección de memoria utilizando el especificador de formato %p para imprimir el valor del puntero
}

int main() {
	int op1, op2, resultado;

	system("clear");

	num_entero();
	num_decimal();
	num_int_char();
	array_hexa();
	clean_ANSI();
	leer_linea();
 	hex_mayus();
	direcc_mem();


	op1 = preguntar_op();
	op2 = preguntar_op();
	mostrar_suma(op1, op2, resultado);
	return EXIT_SUCCESS;

}
