#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdio_ext.h>

#define MAX 30

void leer_vector() {
	int tamano_vector; //Variable para el tamaño del vector
	int vector[tamano_vector]; //Array para el vector

	//Pedimos al usuario el tamaño del vector y lo guardamos en la direccion de memoria de tamaño_vector
	printf("Introduce el tamaño del vector: ");
	scanf("%i", &tamano_vector);
	
	//Imprimimos los números 
	for (float i=0; i<=tamano_vector; i++) {
		printf("%.2f, ", i);
	}
	printf("\n\n");
}

void copo_nieve() {

	for (int x=1; x<=100; x++) {
		sleep(1);

		for (int i=1; i<=1; i++) { //Este no funciona
		printf("\033[L");
		printf("*");

		}
	}
}

void getchar_corchetes() {
	
}

void reales() {
	float num; //Variable para guardar el numero

	for(int i=1; i<=10; i++) {
		
		//Pedimos al usuario un numero real y lo guardamos en la direccion de memoria de num
		printf("Introduce un número real: ");
		scanf(" %f", &num);
		
		//Imprimimos el numero con 6 numeros en la parte entera y 2 en la decimal
		printf("%09.2f \n", num);
	}
	printf("\n\n");
}

void nombre_centrado() {
	char nombre[MAX]; //Array para guardar el nombre

	__fpurge(stdin); //Purgamos el stdin
	//Pedimos al usuario un nombre y lo guardamos en el array
	printf("Introduce un nombre: ");
	fgets(nombre, MAX, stdin);
	
	//printamos el nombre en el centro de la pantalla
	printf("|%505s|", nombre);
}

void nombre_caracter() {
	char nombre[MAX]; //Array para guardar el nombre
	char caracter; //Variable para guardar el caracter
	int longitud_nombre; //Variable para guardar el tamaño del nombre
	int longitud_nombre_mod; //Variable necesaria donde restamos longitud_nombre -1
	
	__fpurge(stdin); //Purgamos stdin

	//Pedimos al usuario que introduzca un nombre y lo guardamos en el array
	printf("Introduce tu nombre: ");
	fgets(nombre, MAX, stdin);

	//Pedimos al usuario que introduzca un caracter y lo guarde en la direccion de memoria de caracter
	printf("Introduce un caracter: ");
	scanf("%c", &caracter);

	//for para imprimir 3 primeros asteriscos de la línea superior
	for (int i=1; i<=3; i++){
		printf("%c", caracter);
	}
	
	//Igualamos la variable longitud_nonmbre a la longitud que ocupa el nombre, esto lo conseguimos con strlen
	longitud_nombre = strlen(nombre);
	
	//for para imprimir tantos caracteres como ocupe el nombre para la línea superior
	for (int i=1; i<longitud_nombre; i++) {
		printf("%c", caracter);
	}

	//for para imprimir los tres últimos caracteres de la línea superior
	for (int i=1; i<=3; i++){
                printf("%c", caracter);
        }
	
	//Imprimimos un espacio
	printf("\n");
	
	//for para poner todas las letras del nombre en mayusculas
	for(int i=0; i<longitud_nombre; i++) {
		nombre[i] = toupper(nombre[i]);
	}
	
	//Imprimimos un caracter y dos espacios antes del nombre (estetica)
	printf("%c  ", caracter);

	//Igualamos longitud_nombre_mod a la longitud del nombre menos 1, esto es para que no se impriman caracteres de más
	longitud_nombre_mod = longitud_nombre -1;

	//for para imprimir el nombre
	for(int i=0; i<longitud_nombre_mod; i++) {
		printf("%c", nombre[i]);
	}

	//Imprimos dos espacios y un caracter despues del nombre (estetica)
	printf("  %c\n", caracter);

	//A partir de aqui es igual que en la lína superior pero en la inferior
	for (int i=1; i<=3; i++){
                printf("%c", caracter);
        }

        for (int i=1; i<longitud_nombre; i++) {
                printf("%c", caracter);
        }

        for (int i=1; i<=3; i++){
                printf("%c", caracter);
        }
	
	printf("\n\n");
}

void array_suma() {

}

void cuadrado() {
	int lado; //Variable para el lado del cuadrado

	//Pedimos al usuario la longitud del lado y la guardamos en la direccion de memoria de lado
	printf("Introduce la longitud del lado del cuadrado: ");
	scanf("%i", &lado);

	for (int i=1; i<=lado; i++) {
		
		for(int x=1; x<=lado; x++) {
			if (i==1 || i==lado || x==1 || x==lado) {
				printf("*");
			}
			else {
				printf(" ");
			}

		}
		printf("\n");
	}

	printf(" \n");
}

int main(){
	
	leer_vector();
	reales();
	nombre_centrado();
	nombre_caracter();
	cuadrado();

	return EXIT_SUCCESS;

}

