#include <stdio.h>
#include <stdlib.h>

int main(){
	unsigned nceldas;
	double *v = NULL;

	printf("Dimensión del vector: ");
	scanf(" %u", &nceldas);

	//Reservar tantos bytes como celdas haya * el numero de bytes que ocupa un double
	v = (double *) malloc (nceldas * sizeof (double));
	
	//Introducir las números del vector
	for (unsigned p=0; p<nceldas; p++) {
		printf("v[%u] = ", p+1);
		scanf(" %lf", v + p);
	}

	//Imprimir vector
	for (unsigned p=0; p<nceldas; p++)
		printf(" %.2lf", *(v+p)); // v[p]

	printf("\n");

	//Liberar los bytes reselvados
	free (v;)
	
	return EXIT_SUCCESS;

}

