#include <stdio.h>
#include <stdlib.h>

int main () {
	int l;

	printf("Introduce la longitud del cuadrado: ");
	scanf("%i", &l);

	for (int i=0; i<2; i++) {
		printf("+");
		for(int y=0; y<l; y++) {
			printf("--");
		}
		printf("+\n");	
		if (i != 1) {
			for(int o=0; o<l; o++) {
			printf("|");
			for(int x=0; x<(l*2); x++) {
				printf(" ");
			}
			printf("|\n");
			}
		}
	}
}
