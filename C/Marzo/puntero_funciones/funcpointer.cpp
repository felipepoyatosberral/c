#include <stdio.h>
#include <stdlib.h>

int suma (int op1, int op2);
int rest (int op1, int op2);
int mult (int op1, int op2);
int divi (int op1, int op2);

int (*catalogo[])(int, int) = { &suma, &rest, &mult, &divi };

enum TOp { sum, rst, mlt, dv, TOT_OP };

const char *menu_opt[] = {
	"Suma",
	"Resta",
	"Multiplicación",
	"División",
	NULL
};

void title () {
	system("clear");
	system("toilet -fpagga --gay CALCULATOR");
	printf("\n\n\n");
}

enum TOp menu () {
	unsigned i = 0;
	unsigned elegido;
	const char **p_menu = menu_opt;

	while (*p_menu != NULL) {
		printf("\t\t%u.- %s\n", ++i, *p_menu);
		p_menu++;
	}

	printf("\n\n");
	printf("\tOpcion: ");
	scanf("%u", &elegido);

	return (enum TOp) (elegido - 1);
}

int suma (int op1, int op2) {return op1 + op2;}
int rest (int op1, int op2) {return op1 - op2;}
int mult (int op1, int op2) {return op1 * op2;}
int divi (int op1, int op2) {return op1 / op2;}

int ask_op (const char *pregunta) {
	int result;

	printf("%s: ", pregunta);
	scanf("%i", &result);

	return result;
}


int main () {
	
	int op1, op2, resultado;
	enum TOp chosen;
	
	title();

	chosen = menu();
	op1 = ask_op ("\tOperando 1");
	op2 = ask_op ("\tOperando 2");

	switch (chosen) {
		case sum:
			resultado = suma(op1,op2);
			printf("\tEl resultado es: %i\n", resultado);
		break;
		case rst:
			resultado = rest(op1,op2);
			printf("\tEl resultado es: %i\n", resultado);
		break;
		case mlt:
			resultado = mult(op1,op2);
			printf("\tEl resultado es: %i\n", resultado);
		break;
		case dv:
			resultado = divi(op1,op2);
			printf("\tEl resultado es: %i\n", resultado);
		break;
	}

	return EXIT_SUCCESS;	
}
