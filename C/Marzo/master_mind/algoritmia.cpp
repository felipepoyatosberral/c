#include <stdio.h>
#include <stdlib.h>

#include "algoritmia.h"

void print_menu () {
	system ("clear");
	system ("toilet -fpagga -F border --metal MASTERMIND");
	printf("1.  Introducción del juego\n");
	printf("2.  Reglas del juego\n");
	printf("3.  Jugar\n");
}

int ask_user (int *menu_option) {
	printf("Selecciona la opción: ");
	scanf("%i", menu_option);
	
	return *menu_option;
}

void switch_menu (int *menu_option) {
		do {
		ask_user(menu_option);
		printf("\n");
		if(*menu_option > 0 && *menu_option <=3) {
			switch(*menu_option) {
				case 1:
					printf("El juego consiste en encontrar la combinación de fichas de colores oculta. Comenzando por la parte superior, cada fila de huecos determina un turno de la partida. En cada turno debemos arrastrar fichas de colores en todos los huecos y tocar o hacer clic en los puntos de la parte derecha para descubrir los aciertos.\n\n");
					break;
				case 2:
					printf("1. Regla1\n");
					printf("2. Regla2\n\n");
					break;
			} 
		}
		else
			printf("El número que has introducido no esta entre 1 y 3\n");
		}
			while (*menu_option != 3);		
}
