#ifndef __ALGORITMIA_H__
#define __ALGORITMIA_H__

#ifdef __cplusplus
extern "C"{
#endif 
	void print_menu();
	int ask_user(int *menu_option);
	void switch_menu(int *menu_option);
#ifdef __cplusplus
}
#endif
#endif

