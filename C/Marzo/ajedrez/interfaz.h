#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#ifdef __cplusplus
extern "C"{
#endif 
	void tablero (enum TPieza tablero_negras[MAX][MAX], unsigned fila = 0);
	void preguntar_dato(unsigned *fila, unsigned *col);
#ifdef __cplusplus
}
#endif
#endif
