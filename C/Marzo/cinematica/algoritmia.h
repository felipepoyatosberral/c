#ifndef __ALGORITMIA_H__
#define __ALGORITMIA_H__

#include "general.h"

#ifdef __cplusplus
extern "C"{
#endif 
	void datos(struct TMovil nave);
	int reloj(clock_t *start_t, clock_t *end_t, double *inc_t);
	void calc(struct TMovil nave, double *inc_t);
	int printar(struct TMovil nave);
#ifdef __cplusplus
}
#endif
#endif

