struct TCoord {        
	double x;
	double y;
};

struct TMovil {
	struct TCoord pos;
	struct TCoord vel;
	struct TCoord acl;
};
