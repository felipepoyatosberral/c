#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "algoritmia.h"

int main (int argc, char *argv[]) {
	double inc_t;
    	clock_t start_t, end_t;
	
	void datos(struct TMovil nave);
	int reloj(clock_t &start_t, clock_t &end_t, double &inc_t);
	void calc(struct TMovil nave, double &inc_t);
	int printar(struct TMovil nave);	

    return EXIT_SUCCESS;
}

