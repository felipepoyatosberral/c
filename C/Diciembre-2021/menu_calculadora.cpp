#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>

//// función para hacer sumas de dos números
int suma (int num1, int num2) {
	return num1 + num2;
}

// función para hacer restas de dos números
int resta (int num1, int num2) {
	return num1 - num2;
}

//// función para hacer divisiones de dos números
int division (int num1, int num2) {
	return num1 / num2;
}

// función para hacer multiplicaciones de dos números
int multiplicacion (int num1, int num2) {
	return num1 * num2;
}

//funcion para preguntar los dos numeros que se operan.
int preguntar_num () {
	static int nnum=0;
	int num;
		nnum += 1;
		printf("Introduce el %iº número: \n", nnum);
		scanf(" %i", &num);
		return num;
}

int main () {
	//Declaración de variables de los números a operar, el resultado, la opcion que eliges y la operación que se va a realizar.
	int num1,num2,num3,num4,resultado,opcion;
	char op1;
	
	//Limpieza de la pantalla y título CALCULATOR
	system ("clear");
	system ("toilet -fpagga -F border --gay CALCULATOR");

	//Opciones del menú
	printf("Elige una de las siguientes opciones: \n \n");
	printf("1.   Operación normal.\n");
	printf("2.   Raíz cuadrada.\n \n");
	printf("Introduce el número de la opción que quieras\n");
	scanf(" %i", &opcion);

	//Si el usuario elige la primera opción 
	if (opcion==1)
	{
		//limpieza de la pantalla y título de OPERACIONES
		system("clear");
		system ("toilet -f small Operaciones \n");
		//El programa te pide dos números y un operando
		num1 = preguntar_num();
	
		num2 = preguntar_num();

		printf("Introduce el operando: ");
		scanf(" %c", &op1);

		//El programa compara el valor que hay dentro de op1 para ver que operando hay y saber que operación es la que debe hacer.
		switch(op1) {
			case '*':
				resultado = multiplicacion (num1, num2);
				printf("%i %c %i = %i \n", num1, op1, num2, resultado);
				break;
			case '+':
				resultado = suma (num1, num2);
				printf("%i %c %i = %i \n", num1, op1, num2, resultado);
				break;
			case '-':
				resultado = resta (num1, num2);
				printf("%i %c %i = %i \n", num1, op1, num2, resultado);
				break;
			case '/':
				resultado = division (num1, num2);
				printf("%i %c %i = %i \n", num1, op1, num2, resultado);
				break;
			default:
				printf("El valor que has introducido no es válido. \n");
			}
	}
	//Si el usuario elige la opción 2
	else if (opcion==2)
	{
		//limpieza de la pantalla y título Raíz Cuadrada
		system("clear");
		system ("toilet -f small Raíz Cuadrada \n");
		//El programa te pide el número y el índice de la raíz cuadrada
		printf("Introduce el numero de la raíz cuadrada: ");
		scanf(" %i", &num3);
	
		printf("Introduce el índice de la raíz cuadrada: ");
		scanf(" %i", &num4);

		resultado = num1 ^ (1/num2);
		printf("El resultado de la raíz cuadrada es: %i \n", resultado);
	}
	//Si el usuario no introduce 1 o 2
	else
	{
		printf("El número que has introducido no es válido: \n");
	}
	

	return EXIT_SUCCESS;
}