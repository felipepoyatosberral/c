#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int numero;

	do {
		printf("Numero 1-7: ");
		scanf ("%i", &numero);
	} while (numero <1 || numero >7);

	//for (;;) // Bucle infinito 

	unsigned char i;
	for (i=0x20; i<0x80; i++)
		printf("%i: %c\n", i, i);

	printf("i=%i\n", i);
	return 0;
}