#include <stdio.h>

#include "algoritmo.h"

bool comprobar_linea_diagonal1(unsigned t[N][N], unsigned fila, unsigned jugador) {
        return t[fila][2] == jugador && t[fila+1][1] == jugador && t[fila+2][0] == jugador;
}

bool comprobar_linea_diagonal2(unsigned t[N][N], unsigned fila, unsigned jugador) {
        return t[fila][0] == jugador && t[fila+1][1] == jugador && t[fila+2][2] == jugador;
}

bool comprobar_linea_horizontal(unsigned t[N][N], unsigned fila, unsigned jugador) {
        return t[fila][0] == jugador && t[fila][1] == jugador && t[fila][2] == jugador;
}

bool comprobar_linea_vertical(unsigned t[N][N], unsigned fila, unsigned jugador) {
        return t[0][fila] == jugador && t[1][fila] == jugador && t[2][fila] == jugador;
}

void preguntar_datos(int *fila_user, int *col_user) {
	printf("Introduce en que fila quieres poner tu ficha (1-3): ");
	scanf("%u", fila_user);
	printf("Introduce en que columna quieres poner tu ficha (1-3): ");
	scanf("%u", col_user);
}

void jugador_ganador(unsigned jugador) {
	printf("Ha ganado el jugador %i!!! \n", jugador);
}

