#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>

#include "algoritmo.h"

const char *representa[] = {
	" ",
	"O",
	"x",
};

int main (int argc, char *argv[]) {

	unsigned tablero[N][N];
	int fila_user;
        int col_user;
	unsigned jugador;

	/* Inicialización */
	//llenar el tablero de ceros
	bzero (tablero, sizeof(tablero));
	for (unsigned i=0; i<MAX; i++) {
		pintar(tablero);
		preguntar_datos(&fila_user, &col_user);
	
		if (tablero[fila_user-1][col_user-1]==0) {
			if (i % 2 == 0) {
				jugador=1;
				tablero[fila_user-1][col_user-1]=1;
			}
			else {
				jugador=2;
				tablero[fila_user-1][col_user-1]=2;
			}
		}
		else {
			printf("La coordenada que has introducido no es correcta o ya esta ocupada!!");
			i--;
		}
		system("clear");
		pintar(tablero);

		for (unsigned fila=0; fila<N; fila++) { 
			if ( comprobar_linea_horizontal(tablero, fila, jugador)) {
				jugador_ganador(jugador);
				i=MAX;
			}
			if ( comprobar_linea_vertical(tablero, fila, jugador)) {
                                jugador_ganador(jugador);
                                i=MAX;
                        }
			if ( comprobar_linea_diagonal1(tablero, fila, jugador)) {
                                jugador_ganador(jugador);
                                i=MAX;
                        }
			if ( comprobar_linea_diagonal2(tablero, fila, jugador)) {
                                jugador_ganador(jugador);
                                i=MAX;
                        }
		}


		__fpurge(stdin);	
		getchar();
		system("clear");
	}
	printf("\n");
	return EXIT_SUCCESS;
}


void pintar (unsigned t[N][N]){
    printf ("\n"); 
    printf("\t");
    for (unsigned x=0; x<N; x++)
        printf("━━━╋");

    printf("\n");

    for (unsigned f=0; f<N; f++) {
        printf("\t");

        for (unsigned c=0; c<N; c++) {
                if (t[f][c] == 0)
                        printf("   ┃");
                else
                        printf(" %s ┃", representa[t[f][c]]);
        }


        printf("\n");
	
        //Imprimir linea horizontal
        printf("\t");
                for (unsigned c=0; c<N; c++){
                        printf("━━━╋");
                }
                printf("\n");
    }
    printf("\n");

}
