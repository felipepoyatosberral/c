#include <stdio.h>
#include <stdlib.h>

#define MAX 50

int main () {

	char sdelim;
	char ndelim;

	unsigned posicion = 0;
	double vector[MAX];

	printf("Introduce un vector: ");
	scanf(" %c", &sdelim);

	do {
	
	    scanf(" %lf", &vector[posicion++]);
	    scanf(" %c", &ndelim);

	}while (ndelim == ',');
	
	printf("\n");
	for (unsigned i=0; i<posicion; i++)
		printf(" %.2lf", vector[posicion]);
	
}
