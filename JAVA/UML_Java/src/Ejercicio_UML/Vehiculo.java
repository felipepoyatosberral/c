package Ejercicio_UML;

public class Vehiculo{
	
	public String marca, color, num_bastidor;
	public int kilometros;
	protected int año_fabricacion;

	public Vehiculo(String marca, String color, String num_bastidor, int kilometros, int año_fabricacion) {
		super();
		this.marca = marca;
		this.color = color;
		this.num_bastidor = num_bastidor;
		this.kilometros = kilometros;
		this.año_fabricacion = año_fabricacion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNum_bastidor() {
		return num_bastidor;
	}

	public void setNum_bastidor(String num_bastidor) {
		this.num_bastidor = num_bastidor;
	}

	public int getKilometros() {
		return kilometros;
	}

	public void setKilometros(int kilometros) {
		this.kilometros = kilometros;
	}

	public int getAño_fabricacion() {
		return año_fabricacion;
	}

	public void setAño_fabricacion(int año_fabricacion) {
		this.año_fabricacion = año_fabricacion;
	}

	@Override
	public String toString() {
		return "Vehiculo [marca=" + marca + ", color=" + color + ", num_bastidor=" + num_bastidor + ", kilometros="
				+ kilometros + ", año_fabricacion=" + año_fabricacion + "]";
	}
	
}
