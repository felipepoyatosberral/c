package Ejercicio_UML;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Informacion {
	
	static Scanner leerteclado = new Scanner(System.in);
	
	public static void verelectricos(ArrayList<Vehiculo> vehiculos, ArrayList<Avion> aviones, ArrayList<Coche> coches) {
		
		boolean contador = false;
		
		if(coches==null || coches.size()==0) {
			System.out.println("\nNo existe ningún vehículo creado, por favor cree uno antes.");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}else {
			System.out.println("Los coches eléctricos son los siguientes: \n");
			for(int i=0; i<coches.size(); i++) {
				if((coches.get(i)).getElectrico() == true) {
					System.out.println(i+1 + ".- " + coches.get(i).toString());
					contador = true;
				}
			}
			if(contador == false) {
				System.out.println("No hay ningún vehículo eléctrico.");
				Menu.menu(vehiculos, aviones, coches);
			}
		}
	}
	
	public static void MaxVelocidad(ArrayList<Vehiculo> vehiculos, ArrayList<Avion> aviones, ArrayList<Coche> coches) {
		if(aviones==null || aviones.size()==0) {
			System.out.println("\nNo existe ningún avión creado, por favor cree uno antes.");
			leerteclado.nextLine();
			Menu.menu(vehiculos, aviones, coches);
		}else {
			Collections.sort(aviones);
			System.out.println("El avión más rápido es: \n" + aviones.get(0).toString());
		}
		
	}

	
}
