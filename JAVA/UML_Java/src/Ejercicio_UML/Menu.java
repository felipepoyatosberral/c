package Ejercicio_UML;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
	static Scanner leerteclado = new Scanner(System.in);
	
	public static ArrayList<Vehiculo> vehiculos;
	
	static int user_option, parar;
	
	public static void menu(ArrayList<Vehiculo> vehiculos, ArrayList<Avion> aviones, ArrayList<Coche> coches) {
		do {
			System.out.println("\nSelecciona la opción que quiera.");
			System.out.println("\n 1.- Agregar un Coche. \n 2.- Agregar un Avión. \n 3.- Agregar un Barco.  \n 4.- Comprobar avión más rápido \n 5.- Vehiculos eléctricos \n 6.- Salir \n");
			System.out.print("Elije: ");
			try {
				user_option = leerteclado.nextInt();
				leerteclado.nextLine();
				if(user_option <=6 && user_option>0) {
					switch(user_option) {
					case 1:
						Crear_vehiculos.crear_coche();
						parar = Crear_vehiculos.crear_mas();
					break;
					case 2:
						Crear_vehiculos.crear_avion();
						parar = Crear_vehiculos.crear_mas();
					break;
					case 3:
						Crear_vehiculos.crear_barco();
						parar = Crear_vehiculos.crear_mas();
					break;
					case 4:
						Informacion.MaxVelocidad(vehiculos, aviones, coches);
						parar = Crear_vehiculos.crear_mas();
					break;
					case 5:
						Informacion.verelectricos(vehiculos, aviones, coches);
						parar = Crear_vehiculos.crear_mas();
					break;
					case 6:
						System.out.println("\nMuchas gracias!");
						System.exit(0);
					break;
					}
				}else {
					System.out.println("***El número debe estar entre 1 y 6***\n");
					leerteclado.nextLine();
					Menu.menu(vehiculos, aviones, coches);
				}
			} catch (Exception e) {
				leerteclado.nextLine();
				System.out.println("***El caracter introducido no es un número del 1 al 6***\n");
				leerteclado.nextLine();
				Menu.menu(vehiculos, aviones, coches);
			}
	  }while(parar==0);
		
	  do {
		System.out.println("");
		Crear_vehiculos.ver_vehiculos();
		
		System.out.println("\nMuchas gracias!");
		System.exit(0);
	  }while(parar==1);
	}
}
		