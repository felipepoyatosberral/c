package UI;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.*;

import java.awt.Color;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

import Main.Usuarios;

import java.awt.Font;

@SuppressWarnings("serial")
public class Leccion_screen extends JPanel {

	private Main_frame main_frame;
	private String[] textos;
	private Color color;
	private JTextArea textPane, textUsr;
	private KeyListener keylistener;
	private Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button0, buttonQ,
			buttonW, buttonE, buttonR, buttonT, buttonY, buttonU, buttonI, buttonO, buttonP, buttonA, buttonS, buttonD,
			buttonF, buttonG, buttonH, buttonJ, buttonK, buttonL, buttonÑ, buttonZ, buttonX, buttonC, buttonV, buttonB,
			buttonN, buttonM, buttonComa, buttonPunto, buttonEspacio;
	private int selectDiff, fallos = 0, pulsaciones = 0, maxfallos;
	private int[] tiempoTardado;
	private Estadisticas_screen estadisticas_screen;
	private String fecha;
	private Usuarios usuario;
	private boolean NoComprobar;
	private List<Character> teclasPermitidas = Arrays.asList('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q', 'w',
			'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'ñ', 'z', 'x', 'c',
			'v', 'b', 'n', 'm', ',', '.', ' ');

	public Leccion_screen(Main_frame main_frame) {

		this.main_frame = main_frame;

		Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();

		setLayout(null);
		setSize(tamañoPantalla);

		selectDiff = 0;

		textUsr = new JTextArea();
		textUsr.setFocusable(true);
		textUsr.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textUsr.setBounds(161, 323, tamañoPantalla.width / 2, 190);
		textUsr.setLineWrap(true);

		JLabel labelLeccionFondo = new JLabel("");
		labelLeccionFondo.setOpaque(true);
		labelLeccionFondo.setBounds(0, 0, tamañoPantalla.width, tamañoPantalla.height);
		labelLeccionFondo.setFocusable(false);

		Image img = new ImageIcon("images\\fondo_leccion.jpg").getImage();
		ImageIcon img2 = new ImageIcon(img.getScaledInstance(labelLeccionFondo.getSize().width,
				labelLeccionFondo.getSize().height, Image.SCALE_SMOOTH));
		labelLeccionFondo.setIcon(img2);

		estadisticas_screen = new Estadisticas_screen(this);
		estadisticas_screen.setFocusable(false);

		textPane = new JTextArea();
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textPane.setBounds(161, 91, tamañoPantalla.width / 2, 190);
		textPane.setEditable(false);
		textPane.setLineWrap(true);
		textPane.setFocusable(false);

		add(textPane);
		add(textUsr);

		add(estadisticas_screen);

		button1 = new Button("1");
		button1.setBounds(142, 644, 90, 55);
		button1.setFocusable(false);
		add(button1);

		button2 = new Button("2");
		button2.setLocation(button1.getLocation().x + 100, button1.getLocation().y);
		button2.setFocusable(false);
		add(button2);

		button3 = new Button("3");
		button3.setLocation(button2.getLocation().x + 100, button2.getLocation().y);
		button3.setFocusable(false);
		add(button3);

		button4 = new Button("4");
		button4.setLocation(button3.getLocation().x + 100, button3.getLocation().y);
		button4.setFocusable(false);
		add(button4);

		button5 = new Button("5");
		button5.setLocation(button4.getLocation().x + 100, button4.getLocation().y);
		button5.setFocusable(false);
		add(button5);

		button6 = new Button("6");
		button6.setLocation(button5.getLocation().x + 100, button5.getLocation().y);
		button6.setFocusable(false);
		add(button6);

		button7 = new Button("7");
		button7.setLocation(button6.getLocation().x + 100, button6.getLocation().y);
		button7.setFocusable(false);
		add(button7);

		button8 = new Button("8");
		button8.setLocation(button7.getLocation().x + 100, button7.getLocation().y);
		button8.setFocusable(false);
		add(button8);

		button9 = new Button("9");
		button9.setLocation(button8.getLocation().x + 100, button8.getLocation().y);
		button9.setFocusable(false);
		add(button9);

		button0 = new Button("0");
		button0.setLocation(button9.getLocation().x + 100, button9.getLocation().y);
		button0.setFocusable(false);
		add(button0);

		buttonQ = new Button("Q");
		buttonQ.setLocation(button1.getLocation().x, button1.getLocation().y + 65);
		buttonQ.setFocusable(false);
		add(buttonQ);

		buttonW = new Button("W");
		buttonW.setLocation(button2.getLocation().x, buttonQ.getLocation().y);
		buttonW.setFocusable(false);
		add(buttonW);

		buttonE = new Button("E");
		buttonE.setLocation(button3.getLocation().x, buttonQ.getLocation().y);
		buttonE.setFocusable(false);
		add(buttonE);

		buttonR = new Button("R");
		buttonR.setLocation(button4.getLocation().x, buttonQ.getLocation().y);
		buttonR.setFocusable(false);
		add(buttonR);

		buttonT = new Button("T");
		buttonT.setLocation(button5.getLocation().x, buttonQ.getLocation().y);
		buttonT.setFocusable(false);
		add(buttonT);

		buttonY = new Button("Y");
		buttonY.setLocation(button6.getLocation().x, buttonQ.getLocation().y);
		buttonY.setFocusable(false);
		add(buttonY);

		buttonU = new Button("U");
		buttonU.setLocation(button7.getLocation().x, buttonQ.getLocation().y);
		buttonU.setFocusable(false);
		add(buttonU);

		buttonI = new Button("I");
		buttonI.setLocation(button8.getLocation().x, buttonQ.getLocation().y);
		buttonI.setFocusable(false);
		add(buttonI);

		buttonO = new Button("O");
		buttonO.setLocation(button9.getLocation().x, buttonQ.getLocation().y);
		buttonO.setFocusable(false);
		add(buttonO);

		buttonP = new Button("P");
		buttonP.setLocation(button0.getLocation().x, buttonQ.getLocation().y);
		buttonP.setFocusable(false);
		add(buttonP);

		buttonA = new Button("A");
		buttonA.setLocation(button1.getLocation().x, buttonQ.getLocation().y + 65);
		buttonA.setFocusable(false);
		add(buttonA);

		buttonS = new Button("S");
		buttonS.setLocation(button2.getLocation().x, buttonA.getLocation().y);
		buttonS.setFocusable(false);
		add(buttonS);

		buttonD = new Button("D");
		buttonD.setLocation(button3.getLocation().x, buttonA.getLocation().y);
		buttonD.setFocusable(false);
		add(buttonD);

		buttonF = new Button("F");
		buttonF.setLocation(button4.getLocation().x, buttonA.getLocation().y);
		buttonF.setFocusable(false);
		add(buttonF);

		buttonG = new Button("G");
		buttonG.setLocation(button5.getLocation().x, buttonA.getLocation().y);
		buttonG.setFocusable(false);
		add(buttonG);

		buttonH = new Button("H");
		buttonH.setLocation(button6.getLocation().x, buttonA.getLocation().y);
		buttonH.setFocusable(false);
		add(buttonH);

		buttonJ = new Button("J");
		buttonJ.setLocation(button7.getLocation().x, buttonA.getLocation().y);
		buttonJ.setFocusable(false);
		add(buttonJ);

		buttonK = new Button("K");
		buttonK.setLocation(button8.getLocation().x, buttonA.getLocation().y);
		buttonK.setFocusable(false);
		add(buttonK);

		buttonL = new Button("L");
		buttonL.setLocation(button9.getLocation().x, buttonA.getLocation().y);
		buttonL.setFocusable(false);
		add(buttonL);

		buttonÑ = new Button("Ñ");
		buttonÑ.setLocation(button0.getLocation().x, buttonA.getLocation().y);
		buttonÑ.setFocusable(false);
		add(buttonÑ);

		buttonZ = new Button("Z");
		buttonZ.setLocation(button1.getLocation().x + 65, buttonA.getLocation().y + 65);
		buttonZ.setFocusable(false);
		add(buttonZ);

		buttonX = new Button("X");
		buttonX.setLocation(buttonZ.getLocation().x + 100, buttonZ.getLocation().y);
		buttonX.setFocusable(false);
		add(buttonX);

		buttonC = new Button("C");
		buttonC.setLocation(buttonX.getLocation().x + 100, buttonZ.getLocation().y);
		buttonC.setFocusable(false);
		add(buttonC);

		buttonV = new Button("V");
		buttonV.setLocation(buttonC.getLocation().x + 100, buttonZ.getLocation().y);
		buttonV.setFocusable(false);
		add(buttonV);

		buttonB = new Button("B");
		buttonB.setLocation(buttonV.getLocation().x + 100, buttonZ.getLocation().y);
		buttonB.setFocusable(false);
		add(buttonB);

		buttonN = new Button("N");
		buttonN.setLocation(buttonB.getLocation().x + 100, buttonZ.getLocation().y);
		buttonN.setFocusable(false);
		add(buttonN);

		buttonM = new Button("M");
		buttonM.setLocation(buttonN.getLocation().x + 100, buttonZ.getLocation().y);
		buttonM.setFocusable(false);
		add(buttonM);

		buttonPunto = new Button(".");
		buttonPunto.setLocation(buttonM.getLocation().x + 100, buttonZ.getLocation().y);
		buttonPunto.setFocusable(false);
		add(buttonPunto);

		buttonComa = new Button(",");
		buttonComa.setLocation(buttonPunto.getLocation().x + 100, buttonZ.getLocation().y);
		buttonComa.setFocusable(false);
		add(buttonComa);

		buttonEspacio = new Button(" ");
		buttonEspacio.setSize(400, 40);
		buttonEspacio.setLocation(button4.getLocation().x, buttonZ.getLocation().y + 65);
		buttonEspacio.setFocusable(false);
		add(buttonEspacio);

		add(labelLeccionFondo);
	}

	public void teclas() {

		@SuppressWarnings("unused")
		Highlighter highlighter = textPane.getHighlighter();
		DefaultHighlighter.DefaultHighlightPainter colorVerde = new DefaultHighlighter.DefaultHighlightPainter(
				Color.GREEN);
		DefaultHighlighter.DefaultHighlightPainter colorRojo = new DefaultHighlighter.DefaultHighlightPainter(
				Color.red);
		textUsr.setEditable(true);

		// Mostrar las estadisiticas a 0
		estadisticas_screen.getTextTiempo().setText("0" + String.valueOf(estadisticas_screen.getMins()) + ":"
				+ String.valueOf(estadisticas_screen.getSecs()));
		estadisticas_screen.getTextFallos()
				.setText(String.valueOf(getFallos()) + " / " + estadisticas_screen.getMaxFallos());
		estadisticas_screen.getTextPPM().setText(String.valueOf(0));

		Button[] botones = { button1, button2, button3, button4, button5, button6, button7, button8, button9, button0,
				buttonQ, buttonW, buttonE, buttonR, buttonT, buttonY, buttonU, buttonI, buttonO, buttonP, buttonA,
				buttonS, buttonD, buttonF, buttonG, buttonH, buttonJ, buttonK, buttonL, buttonÑ, buttonZ, buttonX,
				buttonC, buttonV, buttonB, buttonN, buttonM, buttonComa, buttonPunto, buttonEspacio };

		// Pintar todos los botones por si se ha quedado alguno pintado de la anterior leccion
		for (int i = 0; i < botones.length; i++) {
			botones[i].setBackground(Color.WHITE);
		}

		NoComprobar = false;

		textUsr.addKeyListener(keylistener = new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {

				textUsr.setEditable(false);
				if (textPane.getText().toLowerCase().charAt(0) != Character.toLowerCase(e.getKeyChar())
						&& NoComprobar == false) {
					e.consume();
				} else {
					NoComprobar = true;
					estadisticas_screen.getTimer().start();
					// if la tecla presionada es igual al boton borrar, enter ect.
					if (teclasPermitidas.contains(Character.toLowerCase(e.getKeyChar()))) {
						textUsr.setEditable(true);
						setPulsaciones(getPulsaciones() + 1);
						estadisticas_screen.getTextPPM().setText(String.valueOf(getPulsaciones()));
						try {
							// if si la tecla está bien
							if (textPane.getText().toLowerCase().charAt(textUsr.getText().length()) == Character
									.toLowerCase(e.getKeyChar())) {
								try {
									// Pinta texto
									textPane.getHighlighter().addHighlight(textUsr.getText().length(),
											textUsr.getText().length() + 1, colorVerde);
									color = Color.green;
								} catch (BadLocationException e1) {

								}
								// si la tecla está mal
							} else {
								try {
									// Pinta texto
									if (getFallos() < estadisticas_screen.getMaxFallos()) {
										textPane.getHighlighter().addHighlight(textUsr.getText().length(),
												textUsr.getText().length() + 1, colorRojo);
										color = Color.red;
									}
									// aumenta los fallos
									sumarFallos();
								} catch (BadLocationException e1) {

								}
							}
							// Recorre todos los botones y busca cual se ha presionado y lo pinta verde o rojo
							for (int i = 0; i < botones.length; i++) {
								if (botones[i].getText().toLowerCase().charAt(0) == Character.toLowerCase(e.getKeyChar())) {
									botones[i].setBackground(color);
								}
							}
							// si terminas de poner el texto y pones una letra más suela la siguiente exception
						} catch (StringIndexOutOfBoundsException e1) {
							textUsr.setEditable(false);
							e.consume();
							estadisticas_screen.getTimer().stop();
							getEstadisticas_screen().setSecs(0);
							openCompleteScreen();
						}
					} else {
						e.consume();
						textUsr.setEditable(false);
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// pintar botones de blanco
				for (int i = 0; i < botones.length; i++) {
					botones[i].setBackground(Color.WHITE);
				}
			}
		});
	}

	public void sumarFallos() {

		// Si los fallos son menos que los fallos maximos el fallo se suma en uno y se actualiza si no se muestra mensaje
		if (getFallos() < estadisticas_screen.getMaxFallos()-1) {
			setFallos(getFallos() + 1);
			// Si llega al máximo de fallos se muestra mensaje con las estadisticas
		} else {
			try {
				setFallos(getFallos() + 1);
				Button[] botones = { button1, button2, button3, button4, button5, button6, button7, button8, button9,
						button0, buttonQ, buttonW, buttonE, buttonR, buttonT, buttonY, buttonU, buttonI, buttonO,
						buttonP, buttonA, buttonS, buttonD, buttonF, buttonG, buttonH, buttonJ, buttonK, buttonL,
						buttonÑ, buttonZ, buttonX, buttonC, buttonV, buttonB, buttonN, buttonM, buttonComa, buttonPunto,
						buttonEspacio};

				tiempoTardado = estadisticas_screen.getTiempoTardado();
				usuario = main_frame.getLogin().getUsuario();
				
				estadisticas_screen.getTextFallos().setText(String.valueOf(estadisticas_screen.getMaxFallos()) + " / " + String.valueOf(estadisticas_screen.getMaxFallos()));
				textUsr.setEditable(false);

				// Obtener fecha de hoy
				DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
				fecha = dateFormat.format(new Date());

				estadisticas_screen.getTimer().stop();

				// Se pintan todos los botones de blanco por si alguno se queda pintado
				for (int i = 0; i < botones.length; i++) {
					botones[i].setBackground(Color.WHITE);
				}

				// Mostrar mensaje necesitas practicar.
				JOptionPane.showMessageDialog(main_frame,
						usuario.toStringNombre() + " necesitas practicar... estas son tus estadísticas: " + "\nFallos: "
								+ getFallos() + "\nPPM: "
								+ ((getPulsaciones() * 60) / ((estadisticas_screen.getTiempoTotalSecs())))
								+ "\nTiempo: " + "0" + (tiempoTardado[0]-1) + ":" + tiempoTardado[1] + "\nFecha: " + fecha,
						"Número máximo de fallos", JOptionPane.INFORMATION_MESSAGE);

				main_frame.getMenu().setVisible(true);
				main_frame.getLeccion().setVisible(false);
				estadisticas_screen.setSecs(0);
			} catch (Exception e) {

			}
		}
	}

	public void cargartexto() {
		//Carga texto en el textarea del texto a copiar
		textPane.setText(textos[getSelectDiff()]);
	}

	public void openCompleteScreen() {
		main_frame.dispose();
		main_frame.setSize(800, 500);
		main_frame.setUndecorated(false);
		main_frame.setLocationRelativeTo(null);
		main_frame.setResizable(false);
		main_frame.getLeccion().setVisible(false);
		main_frame.getComplete_screen().posicionarComps();
		main_frame.getComplete_screen().addTextCompleteScreen();
		main_frame.getComplete_screen().setVisible(true);
		main_frame.setVisible(true);
	}

	public void setTextos(String[] textos) {
		this.textos = textos;
	}

	public JTextArea getTextUsr() {
		return textUsr;
	}

	public int getSelectDiff() {
		return selectDiff;
	}

	public void setSelectDiff(int selectDiff) {
		this.selectDiff = selectDiff;
	}

	public String[] getTextos() {
		return textos;
	}

	public Main_frame getMain_frame() {
		return main_frame;
	}

	public void setMain_frame(Main_frame main_frame) {
		this.main_frame = main_frame;
	}

	public JTextArea getTextPane() {
		return textPane;
	}

	public Estadisticas_screen getEstadisticas_screen() {
		return estadisticas_screen;
	}

	public int getFallos() {
		return fallos;
	}

	public void setFallos(int fallos) {
		this.fallos = fallos;
	}

	public int getPulsaciones() {
		return pulsaciones;
	}

	public void setPulsaciones(int pulsaciones) {
		this.pulsaciones = pulsaciones;
	}

	public KeyListener getKeylistener() {
		return keylistener;
	}

	public int getMaxfallos() {
		return maxfallos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}