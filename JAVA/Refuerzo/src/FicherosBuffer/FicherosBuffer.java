package FicherosBuffer;

import java.io.*;

public class FicherosBuffer {

	public static void main(String[] args) {
		
		
		try {
			
			File f = new File("Datos.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			
			//Creación de array de nombres String[] nombres = {"Pepe", "Ana", "Raul", "Laura"}
			String[] nombres = new String[4];
			nombres[0] = "Pepe";
			nombres[1] = "Ana";
			nombres[2] = "Raul";
			nombres[3] = "Laura";
			
			//Escribe los datos del array
			for(int i= 0;i<nombres.length;i++) {
				bw.write(nombres[i]);
			}
			
			//Cerrar fichero
			bw.close();
			
		} catch (IOException e) {
		
			System.out.println(e.getMessage());
		}
		

	}

}
