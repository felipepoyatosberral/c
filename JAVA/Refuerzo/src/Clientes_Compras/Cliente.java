package Clientes_Compras;

import java.util.ArrayList;

public class Cliente {
	
	private String clave, nombre, direccion;
	private int edad;
	private ArrayList<Compra> listaCompra;

	public Cliente(String c, String n, String a, int e) {
		clave = c;
		nombre = n;
		direccion = a;
		edad = e;
		listaCompra = new ArrayList<Compra>();
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public ArrayList<Compra> getListaCompra() {
		return listaCompra;
	}

	public void setListaCompra(ArrayList<Compra> listaCompra) {
		this.listaCompra = listaCompra;
	}
	
	
}
