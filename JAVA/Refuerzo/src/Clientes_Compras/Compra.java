package Clientes_Compras;

import java.util.Date;

public class Compra {
	
	private String 	clave, texto, fecha;
	
	public Compra(String clave, String texto, String fecha) {
		this.clave = clave;
		this.texto = texto;
		this.fecha = fecha;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	
	
	
	
}
