package UI;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import Main.Estadisticas;
import Metods.Metods_UI;

import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class Menu_screen extends JPanel {

	private Main_frame main_frame;
	private Estadisticas[] allEstadisticas, twoStads;
	@SuppressWarnings("unused")
	private Estadisticas stadOldest;

	public Menu_screen(Main_frame main_frame) {

		this.main_frame = main_frame;

		Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();

		int anchoPanel = tamañoPantalla.width / 3;
		int altoPanel = tamañoPantalla.height / 2;

		setSize(tamañoPantalla);
		setLayout(null);

		JMenuBar barraBar = new JMenuBar();
		barraBar.setBorder(UIManager.getBorder("Menu.border"));
		barraBar.setBounds(0, 0, tamañoPantalla.width, 49);
		add(barraBar);

		JMenu menu = new JMenu("Menú");
		menu.setBorder(UIManager.getBorder("Menu.border"));
		barraBar.add(menu);

		JMenuItem estadisticas = new JMenuItem("Estadísticas");
		estadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = 0;
				// Mostrar estadisticas del usuario iniciado, si tiene dos muestra las dos, si
				// tiene 1 muestra 1, si no te dice que hagas una leccion
				if(twoStads[0] == null) {
					i = 0;
				}else if(twoStads[1] == null) {
					i = 1;
				}else {
					i =2;
				}
				
				switch (i) {
				case 0:
					JOptionPane.showMessageDialog(null,
							main_frame.getLogin().getUsuario().toStringNombre() + " todavía no tienes estadisticas,"
									+ " porfavor prueba a completar una lección",
							"Tus estadisticas", JOptionPane.INFORMATION_MESSAGE);
					break;
				case 1:
					JOptionPane.showMessageDialog(null,
							main_frame.getLogin().getUsuario().toStringNombre() + " tus estadisticas son: "
									+ "\nEstadisticas" + "\n" + twoStads[0],
							"Tus estadisticas", JOptionPane.INFORMATION_MESSAGE);
					break;
				case 2:
					JOptionPane.showMessageDialog(null,
							main_frame.getLogin().getUsuario().toStringNombre() + " tus estadisticas son: "
									+ "\nEstadisticas 1" + "\n" + twoStads[0] + "\nEstadisticas 2" + "\n" + twoStads[1],
							"Tus estadisticas", JOptionPane.INFORMATION_MESSAGE);
					break;
				}

			}
		});
		menu.add(estadisticas);

		JMenuItem acercaDe = new JMenuItem("Acerca de");
		acercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						"Aplicación creada por Felipe Poyatos Berral, versión de la aplicación: 5.4", "Acerca de",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		menu.add(acercaDe);

		JMenuItem salir = new JMenuItem("Salir");
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Metods_UI.preguntarAlSalir();
			}
		});
		menu.add(salir);

		JButton buttonLogout = new JButton("Cerrar Sesión");
		buttonLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Vuelve a abrir panel Login
				main_frame.dispose();
				main_frame.setSize(290, 380);
				main_frame.setResizable(false);
				main_frame.setUndecorated(false);
				main_frame.setLocationRelativeTo(null);
				main_frame.getMenu().setVisible(false);
				main_frame.getLogin().setVisible(true);
				main_frame.setVisible(true);
			}
		});
		buttonLogout.setBounds(tamañoPantalla.width - 140, tamañoPantalla.height - tamañoPantalla.height + 50, 140, 40);
		add(buttonLogout);

		JPanel panelMenu = new JPanel();
		panelMenu.setBounds(tamañoPantalla.width / 3, tamañoPantalla.height / 5, anchoPanel, altoPanel);
		add(panelMenu);
		panelMenu.setLayout(null);

		JLabel labelTittleDificultad = new JLabel("Dificultad");
		labelTittleDificultad.setFont(new Font("Tahoma", Font.BOLD, 35));
		labelTittleDificultad.setBounds((anchoPanel / 3) + (anchoPanel) / 25, (anchoPanel) / 20, 187, 82);
		panelMenu.add(labelTittleDificultad);

		JButton buttonFacil = new JButton("Fácil");
		buttonFacil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Definimos el texto que vamos a imprimir, los minutos maximos y el maximo de
				// fallos
				main_frame.getLeccion().setSelectDiff(0);
				main_frame.getLeccion().getEstadisticas_screen().setMins(1);
				main_frame.getLeccion().getEstadisticas_screen().setMaxFallos(5);
				resetearStads();
			}
		});
		buttonFacil.setFont(new Font("Tahoma", Font.PLAIN, 20));
		buttonFacil.setBounds(127, 267, 97, 41);
		panelMenu.add(buttonFacil);

		JLabel lblEligeLaDificultad = new JLabel("Elige la dificultad que quieras");
		lblEligeLaDificultad.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblEligeLaDificultad.setBounds(172, 113, 369, 82);
		panelMenu.add(lblEligeLaDificultad);

		JButton buttonDificil = new JButton("Difífcil");
		buttonDificil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Definimos el texto que vamos a imprimir, los minutos maximos y el maximo de
				// fallos
				main_frame.getLeccion().setSelectDiff(1);
				main_frame.getLeccion().getEstadisticas_screen().setMins(3);
				main_frame.getLeccion().getEstadisticas_screen().setMaxFallos(3);
				resetearStads();
			}
		});
		buttonDificil.setFont(new Font("Tahoma", Font.PLAIN, 20));
		buttonDificil.setBounds(411, 268, 97, 41);

		panelMenu.add(buttonDificil);

		JLabel labelFondoMenu = new JLabel("");
		labelFondoMenu.setSize(tamañoPantalla);
		Image img = new ImageIcon("images\\fondo_menu.jpg").getImage();
		ImageIcon img2 = new ImageIcon(
				img.getScaledInstance(tamañoPantalla.width, tamañoPantalla.height, Image.SCALE_SMOOTH));
		labelFondoMenu.setIcon(img2);
		labelFondoMenu.setBounds(0, 50, tamañoPantalla.width, tamañoPantalla.height);
		add(labelFondoMenu);
	}

	public void resetearStads() {
		// Resetamos los valores de la anterior lección a 0
		main_frame.getMenu().setVisible(false);
		main_frame.getLeccion().setVisible(true);
		main_frame.getLeccion().cargartexto();
		main_frame.getLeccion().getTextUsr().setText("");
		main_frame.getLeccion().getEstadisticas_screen().getTextTiempo().setText("");
		main_frame.getLeccion().getEstadisticas_screen().getTextFallos().setText("");
		main_frame.getLeccion().setFallos(0);
		main_frame.getLeccion().setPulsaciones(0);
		main_frame.getLeccion().getEstadisticas_screen().setSecsTardados(0);
		main_frame.getLeccion().getEstadisticas_screen().setMinsTardados(0);
		main_frame.getLeccion().getTextUsr().setEnabled(true);
		main_frame.getLeccion().getTextUsr().removeKeyListener(main_frame.getLeccion().getKeylistener());
		main_frame.getLeccion().getEstadisticas_screen().getTextPPM().setText("");
		main_frame.getLeccion().teclas();
	}

	public void recogerEstadisticasUser() {
		setAllEstadisticas(main_frame.getEstadisticas());
		allEstadisticas = getAllEstadisticas();
		// array para sacar las dos estadisticas del usuario iniciado
		twoStads = new Estadisticas[2];
		if (allEstadisticas == null) {

		} else {
			// Obtener las dos estadisticas de ese usuario iniciado
			for (int i = 0, x = 0; i < allEstadisticas.length; i++) {
				if (main_frame.getLogin().getUsuario().toStringNombre()
						.equals(allEstadisticas[i].toStringEstadsUser())) {
					twoStads[x] = allEstadisticas[i];
					x++;
				}
			}
		}
	}

	public Estadisticas[] getAllEstadisticas() {
		return allEstadisticas;
	}

	public void setAllEstadisticas(Estadisticas[] allEstadisticas) {
		this.allEstadisticas = allEstadisticas;
	}

	public Estadisticas getStadOldest() {
		return stadOldest;
	}

	public void setStadOldest(Estadisticas stadOldest) {
		this.stadOldest = stadOldest;
	}
}
