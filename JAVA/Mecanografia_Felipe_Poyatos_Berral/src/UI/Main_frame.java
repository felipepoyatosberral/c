package UI;

import java.awt.CardLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import Main.Estadisticas;
import Main.Usuarios;
import Metods.Metods_UI;
import Metods.Metods_ficheros;

@SuppressWarnings("serial")
public class Main_frame extends JFrame{
	
	private Loading_screen inicio;
	private Login_screen login;
	private Menu_screen menu;
	private Leccion_screen leccion;
	private File fichero_usuarios;
	private File fichero_textos;
	private File fichero_estadisticas;
	private Usuarios[] usuarios = null;
	private Estadisticas[] estadisticas = null;
	private Complete_screen complete_screen;
	
	public Main_frame(){
		
		//Características del JFrame
		setUndecorated(true);
		setTitle("MecanoPipe");
		setBounds(100, 100, 500, 320);
		// Mostrar mensaje preguntando si quieres salir
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing (WindowEvent e) {
				Metods_UI.preguntarAlSalir();
			}
		});
		setLocationRelativeTo(null);
		setResizable(false);
		setTitle("MecanoPipe");
		getContentPane().setLayout(new CardLayout(0, 0));
		setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_loading.png"));
		
		//Crear paneles
		inicio = new Loading_screen(this);
		login = new Login_screen(this);
		menu = new Menu_screen(this);
		leccion = new Leccion_screen(this);
		complete_screen = new Complete_screen(this);
				
		//Añadir paneles al JFrame
		add(inicio);
		add(login);
		add(menu);
		add(leccion);
		add(complete_screen);
		
		setVisible(true);
		
		temporizadorInicio();
		
	}
	
	public void temporizadorInicio() {
		
		Timer timer = new Timer();
		
		timer.scheduleAtFixedRate(new TimerTask() {
			int i = 0;
			
			@Override
			public void run() {
				
				inicio.progressBar(i);
				i+=1;
				
				//Comprobar existencia y consistencia de los ficheros
				if(i >= 80) {
					fichero_usuarios = Metods_ficheros.comprobar_ficheros("ficheros_txt\\usuarios.txt");
					fichero_textos = Metods_ficheros.comprobar_ficheros("ficheros_txt\\textos.txt");
					fichero_estadisticas = Metods_ficheros.comprobarFicheroEstads("ficheros_txt\\estadisticas.txt");
				}
				
				if(i > 100) {
					timer.cancel();
					
					//Leer ficheros y guardarlos.
					usuarios = Metods_ficheros.leer_fichero_Usuarios(fichero_usuarios);
					
					leccion.setTextos(Metods_ficheros.leer_fichero_Textos(fichero_textos));
					
					estadisticas = Metods_ficheros.leer_fichero_Estadisiticas(fichero_estadisticas);
					
					//Mostrar ventana login
					dispose();
					setSize(290, 380);					
					setResizable(false);
					
					setUndecorated(false);
					setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_login.png"));
					
					remove(inicio);
					inicio.setVisible(false);
					login.setVisible(true);
					
					setVisible(true);
				}
			}
		}, 0, 60);
	}

	public Loading_screen getInicio() {
		return inicio;
	}

	public void setInicio(Loading_screen inicio) {
		this.inicio = inicio;
	}

	public Login_screen getLogin() {
		return login;
	}

	public void setLogin(Login_screen login) {
		this.login = login;
	}

	public Menu_screen getMenu() {
		return menu;
	}

	public void setMenu(Menu_screen menu) {
		this.menu = menu;
	}

	public Usuarios[] getUsuarios() {
		return usuarios;
	}
	
	public void setUsuarios(Usuarios[] usuarios) {
		this.usuarios = usuarios;
	}	

	public Estadisticas[] getEstadisticas() {
		return estadisticas;
	}

	public void setEstadisticas(Estadisticas[] estadisticas) {
		this.estadisticas = estadisticas;
	}

	public Leccion_screen getLeccion() {
		return leccion;
	}

	public void setLeccion(Leccion_screen leccion) {
		this.leccion = leccion;
	}

	public Complete_screen getComplete_screen() {
		return complete_screen;
	}

	public File getFichero_estadisticas() {
		return fichero_estadisticas;
	}
}