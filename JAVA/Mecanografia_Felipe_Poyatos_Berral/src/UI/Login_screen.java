package UI;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Main.Usuarios;
import Metods.Metods_ficheros;
import javax.swing.JSeparator;
import java.awt.Color;

@SuppressWarnings("serial")
public class Login_screen extends JPanel{
	
	private JTextField textField;
	private JPasswordField passwordField;
	@SuppressWarnings("unused")
	private Main_frame main_frame;
	private Usuarios usuario;

	public Login_screen(Main_frame main_frame) {
		setBackground(new Color(255, 255, 255));
		
		this.main_frame = main_frame;
		
		setLayout(null);
		
		JLabel lbl_login_tittle = new JLabel("Iniciar Sesión");
		lbl_login_tittle.setFont(new Font("Arial Black", Font.BOLD, 25));
		lbl_login_tittle.setBounds(47, 33, 231, 56);
		
		
		JLabel lbl_name = new JLabel("Nombre");
		lbl_name.setBounds(58, 118, 62, 14);
		
		
		textField = new JTextField();
		textField.setToolTipText("Usuario");
		textField.setBorder(null);
		textField.setBounds(150, 115, 86, 20);
		textField.setColumns(10);
		
		JLabel lbl_passwd = new JLabel("Contraseña");
		lbl_passwd.setBounds(58, 183, 82, 14);
		
		
		JButton login_button = new JButton("Entrar");
		login_button.setBackground(new Color(255, 255, 255));
		login_button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Si devuelve null quiere decir que usuario incorrecto, si devuelve usuario iniciado usuario correcto
				if(Metods_ficheros.comprobar_user(main_frame.getUsuarios(), textField.getText(), passwordField.getPassword()) != null) {
					//Guardamos usuario iniciado
					usuario = Metods_ficheros.comprobar_user(main_frame.getUsuarios(), textField.getText(), passwordField.getPassword());
					//Abrimos ventana menu
					main_frame.getLogin().setVisible(false);
					main_frame.getMenu().setVisible(true);
					main_frame.dispose();
					Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();
					main_frame.setSize(tamañoPantalla);
					main_frame.setResizable(false);
					main_frame.setLocationRelativeTo(null);
					main_frame.setUndecorated(true);
					main_frame.setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_menu.png"));
					main_frame.setVisible(true);
					textField.setText("");
					passwordField.setText("");
					main_frame.getMenu().recogerEstadisticasUser();
				}else {
					JOptionPane.showMessageDialog(null, "El usuario o la contraseña introducida no exiten", "Usuario Incorrecto!!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		login_button.setBounds(94, 266, 89, 23);
		
		
		passwordField = new JPasswordField();
		passwordField.setToolTipText("Contraseña");
		passwordField.setBorder(null);
		passwordField.setBounds(150, 180, 86, 20);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(150, 135, 86, 14);
		add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(150, 200, 86, 14);
		add(separator_1);
		
		add(lbl_login_tittle);
		add(lbl_name);
		add(textField);
		add(lbl_passwd);
		add(login_button);
		add(passwordField);
		
		JCheckBox mostrar_passwd = new JCheckBox("Mostrar");
		mostrar_passwd.setBackground(new Color(255, 255, 255));
		mostrar_passwd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mostrar_passwd.isSelected()) {
					passwordField.setEchoChar((char)0);
					mostrar_passwd.setText("Ocultar");
				}else {
					passwordField.setEchoChar('*');
					mostrar_passwd.setText("Mostrar");
				}
			}
		});
		mostrar_passwd.setBounds(150, 216, 97, 23);
		add(mostrar_passwd);
		
		setVisible(true);
		
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}

	public Usuarios getUsuario() {
		return usuario;
	}
	
}