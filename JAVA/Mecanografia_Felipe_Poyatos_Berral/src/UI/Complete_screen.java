package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import Main.Estadisticas;
import Main.Usuarios;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class Complete_screen extends JPanel{
	
	private Main_frame main_frame;
	private Usuarios usuario;
	private JLabel titleEnhorabuena;
	private JTextArea textEstadisticas;
	private JButton buttonSalir, buttonGuardarYEnviar;
	private String fecha;
	private int[] tiempoTardado;
	private Estadisticas estadisticasNuevas;
	

	public Complete_screen (Main_frame main_frame) {
		
		this.main_frame = main_frame;
		
		setVisible(false);
		setBackground(Color.WHITE);
		setLayout(null);
		
		titleEnhorabuena = new JLabel();
		titleEnhorabuena.setHorizontalAlignment(SwingConstants.CENTER);
		titleEnhorabuena.setBackground(Color.blue);
		titleEnhorabuena.setFont(new Font("Tahoma", Font.BOLD, 24));
		add(titleEnhorabuena);
		
		textEstadisticas = new JTextArea(); 
		textEstadisticas.setFont(new Font("Tahoma", Font.PLAIN, 24));
		textEstadisticas.setEditable(false);
		add(textEstadisticas);
		
		buttonSalir = new JButton("Salir Sin Guardar");
		buttonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				volverAlMenu();
			}
		});
		buttonSalir.setFont(new Font("Tahoma", Font.PLAIN, 20));
		add(buttonSalir);
		
		buttonGuardarYEnviar = new JButton("Guardar Y Enviar");
		buttonGuardarYEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enviarCorreo();
				volverAlMenu();
			}
		});
		buttonGuardarYEnviar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		add(buttonGuardarYEnviar);
	}
	
	public void posicionarComps() {
		
		setBounds(0, 0, main_frame.getWidth(), main_frame.getHeight());
		getTitleEnhorabuena().setBounds(0, 20, getWidth(), 30);
		getTextEstadisticas().setBounds(30, getTitleEnhorabuena().getLocation().y+60, main_frame.getWidth()-80, main_frame.getHeight()/2);
		buttonSalir.setBounds(30, getTextEstadisticas().getHeight()+120, 200, 50);
		buttonGuardarYEnviar.setBounds(buttonSalir.getLocation().x+500, buttonSalir.getLocation().y, 200, 50);
	}
	
	public void addTextCompleteScreen() {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		fecha = dateFormat.format(new Date());
		
		tiempoTardado = main_frame.getLeccion().getEstadisticas_screen().getTiempoTardado();
		usuario =  main_frame.getLogin().getUsuario();
		
		getTitleEnhorabuena().setText("¡¡Enhorabuena " + usuario.toStringNombre() + "!!");
		getTextEstadisticas().setText("Estas son tus estadísticas: "
		+ "\n\nUsuario: " + usuario.toStringNombre()
		+ "\nFallos: " + main_frame.getLeccion().getFallos()
		+ "\nPPM: " + ((main_frame.getLeccion().getPulsaciones() * 60) / ((main_frame.getLeccion().getEstadisticas_screen().getTiempoTotalSecs())))
		+ "\nTiempo: " + "0" + tiempoTardado[0] + ":" + tiempoTardado[1]
		+ "\nFecha: " + fecha);
		
		estadisticasNuevas = new Estadisticas (new Date(), usuario.toStringNombre(), 2, (tiempoTardado[0] + ":" + tiempoTardado[1]), main_frame.getLeccion().getFallos());
	}
	
	public void volverAlMenu() {
		main_frame.getMenu().setVisible(true);
		main_frame.getComplete_screen().setVisible(false);
		main_frame.dispose();
		Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();
		main_frame.setSize(tamañoPantalla);
		main_frame.setLocationRelativeTo(null);
		main_frame.setUndecorated(true);
		main_frame.setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_menu.png"));
		main_frame.setVisible(true);
	}
	
	public void enviarCorreo() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		fecha = dateFormat.format(new Date());
		
		tiempoTardado = main_frame.getLeccion().getEstadisticas_screen().getTiempoTardado();
		usuario =  main_frame.getLogin().getUsuario();
		  
		  String host="smtp.gmail.com";  
		  String user="poyatospipe@gmail.com";  
		  String password="cnytjqhfrwqjowji"; 
		  
		  //Coge el correo del usuario iniciado.
		  String to=usuario.toStringCorreo();  
		  
		   Properties props = new Properties();  
		   props.put("mail.smtp.host",host);  
		   props.put("mail.smtp.auth", "true");
		   props.put("mail.smtp.starttls.enable", "true");
		     
		   Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {  
		      protected PasswordAuthentication getPasswordAuthentication() {  
		    return new PasswordAuthentication(user,password);  
		      }  
		    });  
		  
		   
		   //Componer el mensaje 
		    try {  
		     MimeMessage message = new MimeMessage(session);  
		     message.setFrom(new InternetAddress(user));  
		     message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
		     message.setSubject("Estadisiticas Mecanografía");  
		     message.setText(usuario.toStringNombre() + " aquí están tus estadisticas: "
		    			+ "\n\nUsuario: " + usuario.toStringNombre()
		    			+ "\nFallos: " + main_frame.getLeccion().getFallos()
		    			+ "\nPPM: " + ((main_frame.getLeccion().getPulsaciones() * 60) / ((main_frame.getLeccion().getEstadisticas_screen().getTiempoTotalSecs())))
		    			+ "\nTiempo: " + "0" + (tiempoTardado[0]-1) + ":" + tiempoTardado[1]
		    			+ "\nFecha: " + fecha);  
		       
		    //Enviar mensaje
		     Transport.send(message);   
		     JOptionPane.showMessageDialog(null, "Estadisticas enviadas correctamente!!","Estadisticas enviadas", JOptionPane.INFORMATION_MESSAGE);
		     } catch (MessagingException e) {
		    	 JOptionPane.showMessageDialog(null, "No se ha podido enviar el correo, porfavor intentelo de nuevo!","Error al enviar correo", JOptionPane.ERROR_MESSAGE);
		     }
	}

	public JLabel getTitleEnhorabuena() {
		return titleEnhorabuena;
	}

	public JTextArea getTextEstadisticas() {
		return textEstadisticas;
	}

	public Estadisticas getEstadisticasNuevas() {
		return estadisticasNuevas;
	}

	public void setEstadisticasNuevas(Estadisticas estadisticasNuevas) {
		this.estadisticasNuevas = estadisticasNuevas;
	}	

	
}
