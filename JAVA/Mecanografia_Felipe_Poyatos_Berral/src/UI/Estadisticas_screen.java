package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import Main.Usuarios;

import javax.swing.JSeparator;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Estadisticas_screen extends JPanel{

	private Leccion_screen leccion_screen;
	private int secs = 0, mins = 0, maxFallos, secsTardados = 0, minsTardados = 0, tiempoTotalSecs;
	private int[] tiempoRestante, tiempoTardado;
	private JLabel textTiempo, textPPM, textFallos;
	private Timer timer;
	private Usuarios usuario;
	private String fecha;
	
	public Estadisticas_screen(Leccion_screen leccion_screen) {
		
		this.leccion_screen = leccion_screen;
		
		Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		setBackground(new Color(255, 255, 255));
		setBounds((tamañoPantalla.width/2)+(tamañoPantalla.width/5), (tamañoPantalla.height/2)-(tamañoPantalla.height/4), 445, 533);
		setLayout(null);
		
		JLabel titleEstadisticas = new JLabel("Estadisticas");
		titleEstadisticas.setHorizontalAlignment(SwingConstants.CENTER);
		titleEstadisticas.setFont(new Font("Tahoma", Font.BOLD, 24));
		titleEstadisticas.setBounds(0, 15, this.getWidth(), 30);
		titleEstadisticas.setFocusable(false);
		add(titleEstadisticas);
		
		JLabel titleTiempo = new JLabel("Tiempo Restante");
		titleTiempo.setHorizontalAlignment(SwingConstants.CENTER);
		titleTiempo.setFont(new Font("Tahoma", Font.PLAIN, 24));
		titleTiempo.setBounds(0, titleEstadisticas.getLocation().y+85, this.getWidth(), 30);
		titleTiempo.setFocusable(false);
		add(titleTiempo);
		
		JSeparator separatorTiempo = new JSeparator();
		separatorTiempo.setBounds(130, titleTiempo.getLocation().y+35, 200, 2);
		separatorTiempo.setFocusable(false);
		add(separatorTiempo);
		
		textTiempo = new JLabel("");
		textTiempo.setHorizontalAlignment(SwingConstants.CENTER);
		textTiempo.setFont(new Font("Tahoma", Font.PLAIN, 24));
		textTiempo.setBounds(0, titleTiempo.getLocation().y+50, this.getWidth(), 30);
		textTiempo.setFocusable(false);
		add(textTiempo);
		
		JLabel titlePPM = new JLabel("Pulsaciones por minuto");
		titlePPM.setFont(new Font("Tahoma", Font.PLAIN, 24));
		titlePPM.setHorizontalAlignment(SwingConstants.CENTER);
		titlePPM.setBounds(0, textTiempo.getLocation().y+75, this.getWidth(), 30);
		titlePPM.setFocusable(false);
		add(titlePPM);
		
		JSeparator separatorPPM = new JSeparator();
		separatorPPM.setBounds(104, titlePPM.getLocation().y+35, 241, 2);
		separatorPPM.setFocusable(false);
		add(separatorPPM);
		
		textPPM = new JLabel("0");
		textPPM.setHorizontalAlignment(SwingConstants.CENTER);
		textPPM.setFont(new Font("Tahoma", Font.PLAIN, 24));
		textPPM.setBounds(0, titlePPM.getLocation().y+50, this.getWidth(), 30);
		textPPM.setFocusable(false);
		add(textPPM);
		
		JLabel titleFallos = new JLabel("Fallos");
		titleFallos.setHorizontalAlignment(SwingConstants.CENTER);
		titleFallos.setFont(new Font("Tahoma", Font.PLAIN, 24));
		titleFallos.setBounds(0, textPPM.getLocation().y+75, this.getWidth(), 30);
		titleFallos.setFocusable(false);
		add(titleFallos);
		
		JSeparator separatorFallos = new JSeparator();
		separatorFallos.setBounds(189, titleFallos.getLocation().y+35, 70, 2);
		separatorFallos.setFocusable(false);
		add(separatorFallos);
		
		textFallos = new JLabel("0");
		textFallos.setHorizontalAlignment(SwingConstants.CENTER);
		textFallos.setFont(new Font("Tahoma", Font.PLAIN, 24));
		textFallos.setBounds(0, titleFallos.getLocation().y+50, this.getWidth(), 30);
		textFallos.setFocusable(false);
		add(textFallos);
		
		JButton buttonGuardar = new JButton("Salir");
		buttonGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leccion_screen.getMain_frame().getMenu().setVisible(true);
				leccion_screen.setVisible(false);
				getTimer().stop();
				setSecs(0);
			}
		});
		buttonGuardar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		buttonGuardar.setBounds(145, textFallos.getLocation().y+75, 150, 30);
		buttonGuardar.setFocusable(false);
		add(buttonGuardar);
		
		tiempoJuego();
		
	}
	
	public void tiempoJuego() {
		timer = new Timer (1000, new ActionListener ()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		        //Resta 1 a los segundos que quedan y suma uno a los segundos tardados
		    	setSecs(getSecs()-1);
		        setSecsTardados(getSecsTardados()+1);
		        setTiempoTotalSecs(getTiempoTotalSecs()+1);
		        //Si los segundos y los mins llegan a 0 el timer para y muestra ventana necesitas practicar
		        if(getSecs() <= 0 && getMins() <= 0) {
		        	tiempoTardado = getTiempoTardado();
					usuario = leccion_screen.getMain_frame().getLogin().getUsuario();
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					fecha = dateFormat.format(new Date());
		        	
					timer.stop();
		        	setSecs(0);
		        	
		        	JOptionPane.showMessageDialog(leccion_screen.getMain_frame(),
							usuario.toStringNombre() + " necesitas practicar... estas son tus estadísticas: " + "\nFallos: "
									+ leccion_screen.getFallos() + "\nPPM: "
									+ ((leccion_screen.getPulsaciones() * 60) / ((getTiempoTotalSecs())))
									+ "\nTiempo: " + "0" + (tiempoTardado[0]-1) + ":" + tiempoTardado[1] + "\nFecha: " + fecha,
							"Tiempo acabado", JOptionPane.INFORMATION_MESSAGE);
		        	
		        	leccion_screen.getMain_frame().getMenu().setVisible(true);
		        	leccion_screen.getMain_frame().getLeccion().setVisible(false);
					setSecs(0);
		        //Si los segundos que quedan son 0 los segundos se ponen en 59, los minutos restantes bajan en 1 y los minutos tardados suman en 1
		        }else if(getSecs()<0) {
		        	 setSecs(59);
		        	 setSecsTardados(0);
		        	 setMins(getMins()-1);
		        	 setMinsTardados(getMinsTardados()+1);
		        }
		        
		        //Los dos tiempos se meten en un array y se imprimen en las estadisticas
		        tiempoRestante = new int[2];
		        tiempoTardado = new int[2];
		        tiempoRestante[0] = getSecs();
		        tiempoRestante[1] = getMins();
		        tiempoTardado[0] = getMinsTardados();
		        tiempoTardado[1] = getSecsTardados();
		        textTiempo.setText("0"+String.valueOf(tiempoRestante[1])+":"+String.valueOf(tiempoRestante[0]));
		        getTextFallos().setText(String.valueOf(leccion_screen.getFallos())+" / "+getMaxFallos());
		     }
		});
	}

	public int getMins() {
		return mins;
	}

	public void setMins(int mins) {
		this.mins = mins;
	}

	public int getSecs() {
		return secs;
	}

	public void setSecs(int secs) {
		this.secs = secs;
	}

	public Timer getTimer() {
		return timer;
	}

	public JLabel getTextTiempo() {
		return textTiempo;
	}

	public JLabel getTextFallos() {
		return textFallos;
	}

	public JLabel getTextPPM() {
		return textPPM;
	}

	public int getMaxFallos() {
		return maxFallos;
	}

	public void setMaxFallos(int maxFallos) {
		this.maxFallos = maxFallos;
	}

	public int getSecsTardados() {
		return secsTardados;
	}

	public void setSecsTardados(int secsTardados) {
		this.secsTardados = secsTardados;
	}

	public int getMinsTardados() {
		return minsTardados;
	}

	public void setMinsTardados(int minsTardados) {
		this.minsTardados = minsTardados;
	}

	public int[] getTiempoTardado() {
		return tiempoTardado;
	}

	public int[] getTiempoRestante() {
		return tiempoRestante;
	}

	public int getTiempoTotalSecs() {
		return tiempoTotalSecs;
	}

	public void setTiempoTotalSecs(int tiempoTotalSecs) {
		this.tiempoTotalSecs = tiempoTotalSecs;
	}
	
}
