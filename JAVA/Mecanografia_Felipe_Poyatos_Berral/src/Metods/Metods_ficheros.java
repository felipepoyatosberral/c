package Metods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.swing.JOptionPane;

import Main.Estadisticas;
import Main.Usuarios;

public class Metods_ficheros {

	public static Usuarios[] leer_fichero_Usuarios(File fichero_usuarios) {

		Usuarios[] usuarios = null;

		try {
			FileReader file_read = new FileReader(fichero_usuarios);
			@SuppressWarnings("resource")
			BufferedReader buffer_read = new BufferedReader(file_read);

			String linea = buffer_read.readLine();

			String[] usr = linea.split("-");

			usuarios = new Usuarios[usr.length];

			for (int i = 0; i < usr.length; i++) {
				String[] datos_usr = usr[i].split(";");

				usuarios[i] = new Usuarios(datos_usr[0], datos_usr[1], datos_usr[2]);
			}

			return usuarios;
		} catch (IOException e) {
			return null;
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(null, "Hay un fichero vacío, por favor comprueba la integridad de los mismos",
					"Fichero vacío", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
			return null;
		}

	}

	public static Usuarios comprobar_user(Usuarios[] usuarios, String nombre, char[] passwd) {
		// Comprueba que la contraseña este entre 4 y 10 caracteres
		if (passwd.length >= 4 && passwd.length <= 10) {
			// Si el usuario y la contraseña estan bien devuelve el usuario iniciado
			for (int i = 0; i < usuarios.length; i++) {
				if (nombre.equals(usuarios[i].getNombre())
						&& Arrays.equals(usuarios[i].getPassword().toCharArray(), passwd)) {
					return usuarios[i];
				}
			}
		} else {
			JOptionPane.showMessageDialog(null, "La contraseña debe tener entre 4 y 10 carácteres",
					"Contraseña Inválida", 0);
		}
		return null;
	}

	public static File comprobar_ficheros(String name_fichero) {

		File fichero = null;
		// Comprueba que el fichero exista
		fichero = new File(name_fichero);
		if (!fichero.exists()) {
			JOptionPane.showMessageDialog(null, "Falta un fichero, porfavor comprueba que estén todos los ficheros.",
					"Falta un fichero", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		// Comprueba que el fichero no este vacio
		if (fichero.length() == 0) {
			JOptionPane.showMessageDialog(null, "Hay un fichero vacío, por favor comprueba la integridad de los mismos",
					"Fichero vacío", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		return fichero;
	}

	public static File comprobarFicheroEstads(String name_fichero) {

		File fichero = null;
		// Comprueba que el fichero exista
		fichero = new File(name_fichero);
		if (!fichero.exists()) {
			JOptionPane.showMessageDialog(null, "Falta un fichero, porfavor comprueba que estén todos los ficheros.",
					"Falta un fichero", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		return fichero;
	}

	public static String[] leer_fichero_Textos(File fichero_textos) {
		String[] texto1 = null;
		String[] texto2 = null;
		String[] textos = null;

		try {
			FileReader file_read = new FileReader(fichero_textos);
			@SuppressWarnings("resource")
			BufferedReader buffer_read = new BufferedReader(file_read);

			String linea = buffer_read.readLine();

			String[] txt = linea.split("-");

			texto1 = new String[1];
			texto2 = new String[1];
			textos = new String[2];

			texto1[0] = txt[0];
			texto2[0] = txt[1];

			textos[0] = texto1[0];
			textos[1] = texto2[0];
			// Devuelve en la posicion 0 el texto facil y en la 1 el texto dificil
			return textos;
		} catch (IOException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}

	}

	public static Estadisticas[] leer_fichero_Estadisiticas(File fichero_estadisticas) {
		Estadisticas[] estadisticas = null;

		try {

			FileReader file_read = new FileReader(fichero_estadisticas);
			@SuppressWarnings("resource")
			BufferedReader buffer_read = new BufferedReader(file_read);

			String linea = buffer_read.readLine();
			if (linea == null) {

			} else {
				String[] usr = linea.split("-");

				estadisticas = new Estadisticas[usr.length];

				for (int i = 0; i < usr.length; i++) {
					String[] datos_estadisticas = usr[i].split(";");

					SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					try {
						Date fecha = formato.parse(datos_estadisticas[0]);

						estadisticas[i] = new Estadisticas(fecha, datos_estadisticas[1],
								Integer.parseInt(datos_estadisticas[2]), datos_estadisticas[3],
								Integer.parseInt(datos_estadisticas[4]));
					} catch (ParseException e) {
						JOptionPane.showMessageDialog(null, "Ha habido un error, porfavor reinicia la aplicación",
								"Error Inesperado", JOptionPane.ERROR_MESSAGE);
						System.exit(0);
					}

				}
			}

			// Devuelve un array con todas las estadisticas del fichero.
			return estadisticas;
		} catch (IOException e) {
			return null;
		} catch (NullPointerException e) {
			return null;
		}

	}

}
