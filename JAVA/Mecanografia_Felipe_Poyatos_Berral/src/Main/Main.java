package Main;

import java.awt.EventQueue;

import javax.swing.JOptionPane;

import UI.Main_frame;

public class Main {

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {   
			public void run() {
				try {
					//Crear JFrame
					new Main_frame();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Ha habido un error inesperado, por favor reinicie la aplicación.", "Error Inesperado!!", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
			}
		});
	}
}
