package Main;

public class Usuarios {

	String nombre, password, email;

	public Usuarios(String nombre, String password, String email) {
		super();
		this.nombre = nombre;
		this.password = password;
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Usuarios [nombre=" + nombre + ", password=" + password + ", email=" + email + "]";
	}
	
	public String toStringNombre() {
		return nombre;
	}
	public String toStringCorreo() {
		return email;
	}
}
