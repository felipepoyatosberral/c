CONSIDERACIONES:
- En el texto las tildes las cuenta como fallo, el texto debería no tener tildes para que funcione correctamente.
- El fichero de estadisticas se separa por "-" cada estadistica y por ";" cada variable de la estadistica.
- El fichero de textos se separa por "-" cada texto.
- El fichero de usuarios se separa por "-" cada usuario y por ";" cada variable de usuario.
- Para terminar la lección hay que escribir una letra más de la que sale en el texto de arriba, esta no contara como fallo.
- El programa no escribe en el fichero de estadisticas, pero si lee en el.
- Al entrar en la lección hay que hacer click en el textarea de debajo para poder escribir.