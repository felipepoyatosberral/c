package JBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JBC_update {

	public static void main(String[] args) {
		String jdbc = "jdbc:mysql://";
		String host = "localhost";
		String port = "3306";
		String baseDatos = "felipe_BBDD";
		String urlConnection = jdbc + host + ":" + port + "/" + baseDatos;
		String usr = "root";
		String pwd = "";
		
		Connection miConexion = null;
		PreparedStatement sentencia;
		
		try {
			//ESTABLECER CONEXION - DRIVERMANAGER
			miConexion = DriverManager.getConnection(urlConnection, usr, pwd);
			System.out.println("\n¡Conexión ABIERTA!\n");
			
			//CREAR OBJETO SENTENCIA - STATEMENT
			miConexion.createStatement();
		
		
		String sentenciaSQL = "UPDATE alumno SET nombre_alumno = ?" + " WHERE id_alumno = ?";

		sentencia = miConexion.prepareStatement(sentenciaSQL);
		
			
			sentencia.setString(1, "Luis");
			sentencia.setString(2, "5");
			
			
			sentencia.executeUpdate();
		}catch(SQLException sqle) {
			sqle.printStackTrace();
			
		}

	}

}
