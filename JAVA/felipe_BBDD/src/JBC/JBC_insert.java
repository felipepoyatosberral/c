package JBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class JBC_insert {

	public static void main(String[] args) {
		String jdbc = "jdbc:mysql://";
		String host = "localhost";
		String port = "3306";
		String baseDatos = "felipe_BBDD";
		String urlConnection = jdbc + host + ":" + port + "/" + baseDatos;
		String usr = "root";
		String pwd = "";
		
		Connection miConexion = null;
		PreparedStatement sentencia;
		
		try {
			//ESTABLECER CONEXION - DRIVERMANAGER
			miConexion = DriverManager.getConnection(urlConnection, usr, pwd);
			System.out.println("\n¡Conexión ABIERTA!\n");
			
			//CREAR OBJETO SENTENCIA - STATEMENT
			miConexion.createStatement();
		
		
		String sentenciaSQL = "INSERT INTO alumno (id_alumno, nombre_alumno, apellido1_alumno, apellido2_alumno, DNI, email_alumno, telefono_alumno) VALUES (?, ?, ?, ?, ?, ?, ?)";

		sentencia = miConexion.prepareStatement(sentenciaSQL);
		
			
			sentencia.setString(1, "6");
			sentencia.setString(2, "Manuel");
			sentencia.setString(3, "Gonzalez");
			sentencia.setString(4, "Perez");
			sentencia.setString(5, "90283537K");
			sentencia.setString(6, "manuelgonzalez@gmail.com");
			sentencia.setString(7, "678328316");
			sentencia.executeUpdate();
		}catch(SQLException sqle) {
			sqle.printStackTrace();
			
		}
	}

}
