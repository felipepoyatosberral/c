import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class FirstUI {

	private JFrame frame;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JButton btnNewButton_4;
	private JButton btnNewButton_5;
	private JButton btnNewButton_6;
	private JButton btnNewButton_7;
	private JButton btnNewButton_8;
	private JButton btnNewButton_9;
	private JButton btnNewButton_10;
	private JButton btnNewButton_11;
	private JButton btnNewButton_12;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FirstUI window = new FirstUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FirstUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 655, 472);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(100, 100));
		panel.setBackground(new Color(255, 255, 0));
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		btnNewButton_2 = new JButton("New button");
		panel.add(btnNewButton_2);
		
		btnNewButton = new JButton("New button");
		panel.add(btnNewButton);
		
		btnNewButton_1 = new JButton("New button");
		panel.add(btnNewButton_1);
		
		btnNewButton_7 = new JButton("New button");
		panel.add(btnNewButton_7);
		
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 255, 0));
		panel_1.setPreferredSize(new Dimension(100, 100));
		frame.getContentPane().add(panel_1, BorderLayout.WEST);
		
		btnNewButton_4 = new JButton("New button");
		panel_1.add(btnNewButton_4);
		
		btnNewButton_5 = new JButton("New button");
		panel_1.add(btnNewButton_5);
		
		btnNewButton_6 = new JButton("New button");
		panel_1.add(btnNewButton_6);
		
		btnNewButton_3 = new JButton("New button");
		panel_1.add(btnNewButton_3);
		
		panel_2 = new JPanel();
		panel_2.setBackground(new Color(64, 0, 128));
		panel_2.setPreferredSize(new Dimension(100, 100));
		frame.getContentPane().add(panel_2, BorderLayout.EAST);
		
		btnNewButton_8 = new JButton("New button");
		panel_2.add(btnNewButton_8);
		
		btnNewButton_9 = new JButton("New button");
		panel_2.add(btnNewButton_9);
		
		btnNewButton_10 = new JButton("New button");
		panel_2.add(btnNewButton_10);
		
		panel_3 = new JPanel();
		panel_3.setBackground(new Color(255, 128, 0));
		panel_3.setPreferredSize(new Dimension(100, 100));
		frame.getContentPane().add(panel_3, BorderLayout.SOUTH);
		
		btnNewButton_12 = new JButton("New button");
		panel_3.add(btnNewButton_12);
		
		btnNewButton_11 = new JButton("New button");
		panel_3.add(btnNewButton_11);
		
		panel_4 = new JPanel();
		panel_4.setBackground(new Color(255, 0, 0));
		frame.getContentPane().add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(null);
		
		JButton btnNewButton_13 = new JButton("New button");
		btnNewButton_13.setBounds(89, 27, 89, 23);
		panel_4.add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("New button");
		btnNewButton_14.setBounds(89, 79, 89, 23);
		panel_4.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("New button");
		btnNewButton_15.setBounds(89, 129, 89, 23);
		panel_4.add(btnNewButton_15);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(33, 31, 46, 14);
		panel_4.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(33, 83, 46, 14);
		panel_4.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setBounds(33, 133, 46, 14);
		panel_4.add(lblNewLabel_2);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(272, 26, 97, 71);
		panel_4.add(textArea);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(272, 125, 97, 71);
		panel_4.add(textArea_1);
	}
}
