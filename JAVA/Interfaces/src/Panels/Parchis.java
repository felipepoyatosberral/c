package Panels;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JPanel;
import java.awt.Color;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Parchis {

	private JFrame frame;
	static private JPanel panel_verde;
	static private JPanel panel_azul;
	static private JPanel panel_rojo;
	static private JPanel panel_amarillo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Parchis window = new Parchis();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Parchis() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 786, 448);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 2, 0, 0));
		
		//Cambiar icono del JFrame.
		Image icono = null;
		try {
			icono = ImageIO.read(new File("src/Panels/icono.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		frame.setIconImage(icono);
		
		panel_verde = new JPanel();
		panel_verde.setBackground(new Color(128, 255, 0));
		frame.getContentPane().add(panel_verde);
		
		JButton btnNewButton = new JButton("Botón 1");
		
		panel_verde.add(btnNewButton);
		
		panel_azul = new JPanel();
		panel_azul.setVisible(false);
		panel_azul.setBackground(new Color(0, 0, 255));
		frame.getContentPane().add(panel_azul);
		
		JButton btnNewButton_1 = new JButton("Botón 2");
		
		panel_azul.add(btnNewButton_1);
		
		panel_rojo = new JPanel();
		panel_rojo.setVisible(false);
		panel_rojo.setBackground(new Color(255, 0, 0));
		frame.getContentPane().add(panel_rojo);
		
		JButton btnNewButton_2 = new JButton("Botón 3");
		
		panel_rojo.add(btnNewButton_2);
		
		panel_amarillo = new JPanel();
		panel_amarillo.setVisible(false);
		panel_amarillo.setBackground(new Color(255, 255, 0));
		frame.getContentPane().add(panel_amarillo);
		
		JButton btnNewButton_3 = new JButton("Botón 4");
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_azul.setVisible(true);
			}
		});
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_rojo.setVisible(true);
			}
		});
		
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_amarillo.setVisible(true);
			}
		});
		
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_azul.setVisible(false);
				panel_rojo.setVisible(false);
				panel_amarillo.setVisible(false);
			}
		});
		panel_amarillo.add(btnNewButton_3);
	}
}
