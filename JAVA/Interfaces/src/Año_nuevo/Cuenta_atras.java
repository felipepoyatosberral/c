package Año_nuevo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JProgressBar;

public class Cuenta_atras {

	private JFrame frame;
	private JPanel panel;
	private JLabel label_navidad;
	private JPanel panel_navidad;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cuenta_atras window = new Cuenta_atras();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Cuenta_atras() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 573, 377);
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(100, 100));
		panel.setBounds(0, 0, 557, 338);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JButton boton_start = new JButton("Empezar");
		
		boton_start.setBounds(228, 118, 106, 58);
		panel.add(boton_start);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setForeground(new Color(128, 255, 0));
		progressBar.setBackground(new Color(255, 0, 0));
		progressBar.setBounds(10, 263, 537, 34);
		panel.add(progressBar);
		
		panel_navidad = new JPanel();
		panel_navidad.setBackground(new Color(0, 255, 255));
		panel_navidad.setBounds(196, 27, 165, 48);
		panel.add(panel_navidad);
		panel_navidad.setVisible(false);
		
		label_navidad = new JLabel("FELIZ NAVIDAD");
		label_navidad.setBackground(new Color(255, 255, 255));
		label_navidad.setForeground(new Color(0, 0, 0));
		label_navidad.setFont(new Font("Cooper Black", Font.PLAIN, 14));
		label_navidad.setBounds(208, 45, 178, 47);
		panel_navidad.add(label_navidad);
		//label_navidad.setVisible(false);
		
		
		boton_start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
}
