package Traductor;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Color;

public class Traductor {

	private JFrame frmTraductor;
	private JTextField textoatraducir;
	private JTextField textotraducido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Traductor window = new Traductor();
					window.frmTraductor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Traductor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTraductor = new JFrame();
		frmTraductor.getContentPane().setBackground(new Color(150, 188, 172));
		frmTraductor.setBackground(new Color(255, 255, 255));
		frmTraductor.setResizable(false);
		frmTraductor.setTitle("Traductor");
		frmTraductor.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\CFGS\\Documents\\FPB_DI\\Interfaces\\src\\Traductor\\icono.png"));
		frmTraductor.setBounds(100, 100, 450, 300);
		frmTraductor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTraductor.getContentPane().setLayout(null);
		
		JLabel label_tittle = new JLabel("TRADUCTOR");
		label_tittle.setFont(new Font("Tahoma", Font.BOLD, 18));
		label_tittle.setBounds(159, 28, 131, 40);
		frmTraductor.getContentPane().add(label_tittle);
		
		JLabel label_textoatraducir = new JLabel("Texto a traducir");
		label_textoatraducir.setBounds(65, 91, 106, 14);
		frmTraductor.getContentPane().add(label_textoatraducir);
		
		textoatraducir = new JTextField();
		textoatraducir.setBounds(193, 88, 158, 48);
		frmTraductor.getContentPane().add(textoatraducir);
		textoatraducir.setColumns(10);
		
		JButton boton_traducir = new JButton("Traducir");
		
		boton_traducir.setBounds(226, 147, 89, 23);
		frmTraductor.getContentPane().add(boton_traducir);
		
		JLabel label_textotraducido = new JLabel("Palabra traducida");
		label_textotraducido.setBounds(65, 189, 106, 14);
		frmTraductor.getContentPane().add(label_textotraducido);
		
		textotraducido = new JTextField();
		textotraducido.setColumns(10);
		textotraducido.setBounds(193, 181, 158, 48);
		frmTraductor.getContentPane().add(textotraducido);
		
		boton_traducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textoatraducir.getText();
				traducir(textoatraducir.getText());
			}
			
		});

	}
	
	private void traducir(String text) {
		Document web = null;
		String url="https://www.spanishdict.com/translate/"+text;
		try {
			web = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements palabras= web.getElementById("quickdef1-es").getElementsByClass("a--tds4TDh9");
		
		textotraducido.setText(palabras.html());
	}
}

