package pruebas_mecanografia;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.BoxLayout;
import javax.swing.JProgressBar;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class ui {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ui window = new ui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 924, 563);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, "name_7278225613900");
		panel.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 908, 56);
		menuBar.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(menuBar);
		
		JMenu mnNewMenu = new JMenu("Dificultad");
		mnNewMenu.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Fácil");
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Difícil");
		mntmNewMenuItem_1.setHorizontalAlignment(SwingConstants.CENTER);
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenu mnNewMenu_1 = new JMenu("Estadísticas");
		menuBar.add(mnNewMenu_1);
		
		JMenu mnNewMenu_3 = new JMenu("Acerca de");
		menuBar.add(mnNewMenu_3);
		
		JButton btnNewButton = new JButton("Logout");
		btnNewButton.setBounds(791, 57, 117, 44);
		panel.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("images\\fondo_menu.jpg"));
		lblNewLabel.setBounds(0, 0, 908, 524);
		panel.add(lblNewLabel);
		
		/*//Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(1000, 1000);
		
		JMenuBar barraMenu = new JMenuBar();
		barraMenu.setBounds(380, 7, 150, 28);
		barraMenu.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		JMenu menuEstadisticas = new JMenu("Estadísticas");
		menuEstadisticas.setBorder(UIManager.getBorder("CheckBox.border"));
		
		JMenu menuAcercade = new JMenu("Acerca de");
		menuAcercade.setBorder(UIManager.getBorder("CheckBox.border"));
		
		JButton buttonLogout = new JButton("Logout");
		buttonLogout.setBounds(540, 7, 65, 23);
		
		JLabel labelFondoMenu = new JLabel("");
		labelFondoMenu.setBounds(615, 19, 0, 0);
		labelFondoMenu.setIcon(new ImageIcon("images\\fondo_menu.jpg"));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 255, 0));
		panel.setBounds(617, 123, -244, 300);
		panel.setVisible(true);
		setLayout(null);
		
		add(barraMenu);
		barraMenu.add(menuEstadisticas);
		barraMenu.add(menuAcercade);
		add(buttonLogout);
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		add(labelFondoMenu);*/
		
		
	}
}
