package Ejercicio5;

public class Ej5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int fact = 25;
		
		System.out.println("El factorial de " + fact + " es: " + factorial(fact));
	
		
	}

	private static int factorial(int fact) {
		
		if(fact==1) {
			return 1;
		}else {
			return fact*factorial(fact-1);
		}
	}

}
