package Ejercicio6;

public class Profesor extends Trabajador{
	
	int asing;
	boolean tutor;
	
	public Profesor(String dni, String nombre, String apellido, int salario, int asing, boolean tutor) {
		super(dni, nombre, apellido, salario);
		this.asing = asing;
		this.tutor = tutor;
		// TODO Auto-generated constructor stub
	}
	public int getAsing() {
		return asing;
	}
	public void setAsing(int asing) {
		this.asing = asing;
	}
	public boolean isTutor() {
		return tutor;
	}
	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}
	
	@Override
	public String toString() {
		return "Profesor [asing=" + asing + ", tutor=" + tutor + ", dni=" + dni + ", nombre=" + nombre + ", apellido="
				+ apellido + ", salario=" + salario + "]";
	}
}
