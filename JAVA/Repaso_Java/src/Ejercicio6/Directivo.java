package Ejercicio6;

public class Directivo extends Trabajador{
	
	int turno; //0 Mañana 1 Tarde
	boolean salesiano;

	public Directivo(String dni, String nombre, String apellido, int salario, int turno, boolean salesiano) {
		super(dni, nombre, apellido, salario);
		this.turno = turno;
		this.salesiano = salesiano;
		// TODO Auto-generated constructor stub
	}

	public int getTurno() {
		return turno;
	}

	public void setTurno(int turno) {
		this.turno = turno;
	}

	public boolean isSalesiano() {
		return salesiano;
	}

	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}

	@Override
	public String toString() {
		return "Directivo [turno=" + turno + ", salesiano=" + salesiano + ", dni=" + dni + ", nombre=" + nombre
				+ ", apellido=" + apellido + ", salario=" + salario + "]";
	}
	
}
