package Ejercicio6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Scanner;

public class Alta {
	
	static Scanner leerteclado = new Scanner(System.in);
	
	static String dni, nombre, apellido, user_parar, estudios, nombre_mod, fechatext;
	static int salario=0, asign=0, parar=0, user_option, turno=0, num_horas;
	static boolean tutor=false, salesiano=false, convalidable, repetidor;
	static double antiguedad=0;
	static Profesor profe;
	static Date fecha_nacimiento;
	static char sexo;
	
	static ArrayList<Trabajador> personas;

	public static void menu(ArrayList<Trabajador> personas) {
		
			while(parar==0) {
				System.out.println("Selección la opción que quiera.");
				System.out.println("\n 1.- Agregar un Profesor. \n 2.- Agregar un Directivo. \n 3.- Agregar un Administrativo.  \n 4.- Agregar Alumno \n 5.- Agregar Módulo \n 6.- Calcular el mayor sueldo \n 7.- Salir");
				try {
					user_option = leerteclado.nextInt();
					leerteclado.nextLine(); //Limpiar /n del buffer
					switch (user_option) {
					case 1:
						crear_profesor();
					break;
					case 2:
						crear_directivo();
					break;
					case 3:
						crear_administrativo();
					break;
					case 4:
						crear_alumno();
					break;
					case 5:
						crear_modulo();
					break;
					case 6:
						maxSueldo();
					break;
					case 7:
						System.out.println("Muchas gracias!");
						parar = 1;
						break;
					default:
						System.out.println("La opción elegida no está contemplada porfavor elija una de las opciones mostradas anteriormente.\n");
						break;
					}
					
				} catch (Exception e) {
					System.out.println("Sólo se admiten opciones numéricas\n");
					leerteclado.nextLine(); //Limpiar /n del buffer
				}
			}
			leerteclado.close();
		}
		
		
	
		private static void crear_alumno() {
			datos_persona();
			
			/* Pedir la fecha de nacimiento */
			System.out.println("Introduce tu fecha de nacimiento (dd/mm/yyyy): ");
			fechatext = leerteclado.nextLine();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
			try {
				fecha_nacimiento = sdf.parse("fechatext");
			} catch (ParseException e) {
				
			}
		
			System.out.println("Introduce tu sexo (m/f): ");
			sexo = leerteclado.next().charAt(0);
			System.out.println("¿El alumno es repetidor? true/false");
			repetidor = leerteclado.nextBoolean();
			
			Alumno al = new Alumno(dni, nombre, apellido, fecha_nacimiento, sexo, repetidor);
			
			System.out.println(al.toString());
			
	}



		private static void crear_modulo() {
			
			System.out.println("Introduce el nombre del módulo: ");
			nombre_mod = leerteclado.nextLine();
			System.out.println("Introduce el número de horas del módulo: ");
			num_horas = leerteclado.nextInt();
			leerteclado.nextLine();
			/* INTRODUCIR DATOS PROFESOR DE LA ASIGNATURA*/
			System.out.println("DATOS DEL PROFESOR");
			Profesor profe = crear_profesor();
			
			System.out.println("¿El módulo es convalidable? ");
			convalidable = leerteclado.nextBoolean();
			
			
			
			Modulo m = new Modulo(nombre_mod, num_horas, profe, convalidable);
			
			System.out.println(m.toString());
	}

		private static void maxSueldo() {
			
			Collections.sort(personas);
			
			System.out.println(personas.get(0).toString());
			
	}

		private static void datos_persona() {
			
			System.out.println("Introduce tu dni: ");
			dni = leerteclado.nextLine();
			System.out.println("Introduce tu nombre: ");
			nombre = leerteclado.nextLine();
			System.out.println("Introduce tu apellido: ");
			apellido = leerteclado.nextLine();
		}

			
		private static void crear_directivo() {
			datos_persona();
			
			System.out.println("Introduce tu salario: ");
			salario = leerteclado.nextInt();
			
			System.out.println("Introduce tu turno: ");
			turno = leerteclado.nextInt();
			System.out.println("¿Eres salesiano? true/false: ");
			salesiano = leerteclado.nextBoolean();
			
			Directivo d = new Directivo(dni, nombre, apellido, salario, turno, salesiano);
			
			System.out.println(d.toString());
			
		}
			
		private static Profesor crear_profesor() {
			datos_persona();	
			
			System.out.println("Introduce el número de asignaturas: ");
			asign = leerteclado.nextInt();
			System.out.println("¿Eres tutor? true/false: ");
			tutor = leerteclado.nextBoolean();
			
			Profesor p = new Profesor(dni, nombre, apellido, salario, asign, tutor);
			
			System.out.println(p.toString());
			
			return p;
		}

		private static void crear_administrativo() {
			datos_persona();
			
			System.out.println("Introduce tus estudios: ");
			estudios = leerteclado.next();
			System.out.println("Introduce tu antiguedad: ");
			antiguedad = leerteclado.nextDouble();
			
			Administrativo a = new Administrativo(dni, nombre, apellido, salario, antiguedad, estudios);
			
			System.out.println(a.toString());
		}

		
		
		
		/*private static void seguir_creando() {
			System.out.println("¿Desea Añadir otro empleado? si/no");
			user_parar = leerteclado.next();
			if(user_parar=="si") {
				parar=0;
			}else if(user_parar=="no") {
				parar=1;
			}else {
				System.out.println("El carácter que ha introducido no es correcto, por favor introduce si o no");
			}
		} */

}
