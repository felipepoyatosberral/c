package Ejercicio6;

import java.util.Date;

public class Alumno extends Persona{
	
	Date fecha_nacimiento;
	char sexo;
	boolean repetidor;

	public Alumno(String dni, String nombre, String apellido, Date fecha_nacimiento, char sexo, boolean repetidor) {
		super(dni, nombre, apellido);
		this.fecha_nacimiento = fecha_nacimiento;
		this.sexo = sexo;
		this.repetidor = repetidor;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	@Override
	public String toString() {
		return "Alumno [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + ", fecha_nacimiento="
				+ fecha_nacimiento + ", sexo=" + sexo + ", repetidor=" + repetidor + "]";
	}
	
	
	
}
