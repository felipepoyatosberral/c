package Ejercicio4;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		
		int tam=0,resultado=0,continuar=0;
		Scanner leerteclado = new Scanner (System.in);
		
		try {
			while (continuar == 0) {
				System.out.println("Introduzca el tamaño de la matriz NxN");
				tam = leerteclado.nextInt();
				int[][] array = new int[tam][tam];

				for (int i = 0; i < array.length; i++) {
					for (int x = 0; x < array.length; x++) {
						array[i][x] = (int) (Math.random() * 100);
						System.out.print(array[i][x] + " ");
						resultado = resultado + array[i][x];

					}
					System.out.println();
				}
				System.out.println("El resultado es: " + resultado);
				System.out.println("¿Desea continuar?(Pulse 0 para 'Sí' o cualquier otra tecla para 'No')");
				continuar = leerteclado.nextInt();
			}
			leerteclado.close();
		} catch (Exception e) {
			System.out.println("Ha ocurrido un error vuelve a intentarlo más tarde");
		}
	}

}
