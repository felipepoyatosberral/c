package Main;

public class Estadisticas {
	
	int ppm, tiempo, fallos;

	public Estadisticas(int ppm, int tiempo, int fallos) {
		super();
		this.ppm = ppm;
		this.tiempo = tiempo;
		this.fallos = fallos;
	}

	public int getPpm() {
		return ppm;
	}

	public void setPpm(int ppm) {
		this.ppm = ppm;
	}

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public int getFallos() {
		return fallos;
	}

	public void setFallos(int fallos) {
		this.fallos = fallos;
	}

	@Override
	public String toString() {
		return "Estadisticas [ppm=" + ppm + ", tiempo=" + tiempo + ", fallos=" + fallos + "]";
	}
	
}
