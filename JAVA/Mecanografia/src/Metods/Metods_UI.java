package Metods;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import UI.Login_screen;
import UI.Main_frame;
import UI.Menu_screen;


public class Metods_UI {
	
	private static Menu_screen menu = new Menu_screen();
	
	public static boolean barra_de_progreso(JProgressBar loadingbar) {
		
		//Funcionalidad progressbar.
		Timer temporizador = new Timer();

        temporizador.scheduleAtFixedRate(new TimerTask() {
            int i = 50;
            boolean x;

			@Override
			public void run() {
				
				    loadingbar.setValue(loadingbar.getValue()+2);
					i--;
					
					Metods_ficheros.comprobar_ficheros(i);

					if(loadingbar.getValue()==80) {
						x=Metods_ficheros.comprobar_ficheros(i);
						
						if(x==true) {
							System.out.println("Funciona");
						}else if(x==false) {
							System.exit(0);
						}
					}
					
					if (i < 0) {
						temporizador.cancel();
					}
			}
        }, 0, 100); 
        return true;
	}
	
	public static void preguntarAlSalir() {
		int i = JOptionPane.showConfirmDialog(null, "¿Seguro que quieres salir?", "Cuidado",JOptionPane.YES_NO_OPTION);
		if(i == JOptionPane.YES_OPTION){
			JOptionPane.showMessageDialog(null, "Gracias por usar la aplicación!!", "Gracias", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
	}
	
	public static void abrir_menu() {
		Frame[] frame = Main_frame.getFrames();
		frame[0].dispose();
		
		//Menu_screen menu = new Menu_screen();
		
		//Características del JFrame
		frame[0].setVisible(true);
		frame[0].setTitle("MecanoPipe");
		Dimension tamañoPantalla = Toolkit.getDefaultToolkit().getScreenSize();
		frame[0].setSize(tamañoPantalla);
		frame[0].setResizable(false);
		frame[0].setLocationRelativeTo(null);
		frame[0].setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_menu.png"));
		((Main_frame) frame[0]).getContentPane().setLayout(null);
		
		
		//Añadir paneles al JFrame
		((Main_frame) frame[0]).getContentPane().add(menu);
		menu.setVisible(true);
	}
	
public static void abrirLogin() {
		
		Frame[] frame = Main_frame.getFrames();
		
		Login_screen login = new Login_screen();
		
		frame[0].dispose();			
		
		frame[0].setSize(290, 380);
		frame[0].setTitle("MecanoPipe");
		// Mostrar mensaje preguntando si quieres salir
		((Main_frame) frame[0]).setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame[0].addWindowListener(new WindowAdapter() {
			public void windowClosing (WindowEvent e) {
				Metods_UI.preguntarAlSalir();
			}
		});
		
		frame[0].setLocationRelativeTo(null);
		frame[0].setResizable(false);
		frame[0].setIconImage(Toolkit.getDefaultToolkit().getImage("images\\icono_login.png"));
		frame[0].setUndecorated(false);
		
		login.setVisible(true);
		
		((Main_frame) frame[0]).getContentPane().remove(menu);
		((Main_frame) frame[0]).getContentPane().add(login);
		
		
		frame[0].setVisible(true);
		
	}
	
}
